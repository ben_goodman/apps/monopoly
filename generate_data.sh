#!/bin/bash

rm -rf results || true
mkdir ./results

for seed in {1..1000}; do
    echo "Running seed $seed"
    ./target/debug/monopoly \
        ./config.yaml \
        --output ./results/$seed.json \
        --seed $seed
done
