use std::fmt;

use crate::game_assets::{cell::CellType, PlayerToken};

#[derive(Debug, PartialEq)]
pub enum GameError {
    PlayerNotFound(PlayerToken),
    CellNotFound(i32),
    OwnershipRequired,
    InvalidCellType(CellType),
    PurchaseableRequired(CellType),
    UpgradeableRequired(CellType),
    InsufficientFunds(PlayerToken, i32),
    InsufficientResources,
    InvalidMove,
    MaxImprovementsReached,
    MinImprovementsReached,
    MinImprovementsRequired,
    MortgageRequired,
    UnMortgageRequired,
    InvalidImprovementTier(i32),
    FailedToExitJail,
    NegativeTransferAmount(i32),
    TradeRejected(PlayerToken, PlayerToken, String, String),
}

#[derive(Debug, PartialEq)]
pub enum GameErrorKind {
    PlayerNotFound,
    CellNotFound,
    OwnershipRequired,
    InvalidCellType,
    PurchaseableRequired,
    UpgradeableRequired,
    InsufficientFunds,
    InvalidMove,
    MaxImprovementsReached,
    MinImprovementsReached,
    MinImprovementsRequired,
    MortgageRequired,
    UnMortgageRequired,
    InvalidImprovementTier,
    FailedToExitJail,
    NegativeTransferAmount,
    TradeRejected,
    InsufficientResources,
}

impl fmt::Display for GameError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            GameError::PlayerNotFound(token) => write!(f, "Player not found: {:?}", token),
            GameError::CellNotFound(loc) => write!(f, "Cell not found: {:?}", loc),
            GameError::InsufficientFunds(token, amount) => {
                write!(
                    f,
                    "Insufficient funds for player {:?} to transfer amount: {}",
                    token, amount
                )
            }
            GameError::InvalidMove => write!(f, "Invalid move"),
            GameError::MaxImprovementsReached => write!(f, "Max improvements reached."),
            GameError::MinImprovementsReached => write!(f, "Min improvements reached."),
            GameError::MinImprovementsRequired => write!(f, "Min improvements required."),
            GameError::UnMortgageRequired => write!(f, "An un-mortgaged property is required."),
            GameError::MortgageRequired => write!(f, "A mortgaged property is required."),
            GameError::InvalidCellType(cell_type) => {
                write!(f, "Invalid cell type encountered: {:?}", cell_type)
            }
            GameError::PurchaseableRequired(cell_type) => {
                write!(f, "This cell type ({:?}) is not Purchaseable.", cell_type)
            }
            GameError::OwnershipRequired => write!(f, "Ownership required."),
            GameError::UpgradeableRequired(cell_type) => {
                write!(f, "This cell type ({:?}) is not Upgradeable.", cell_type)
            }
            GameError::InvalidImprovementTier(tier) => {
                write!(f, "Invalid improvement tier: {}", tier)
            }
            GameError::FailedToExitJail => write!(f, "Failed to exit jail."),
            GameError::NegativeTransferAmount(amount) => {
                write!(f, "Transfer amount cannot be negative: {}", amount)
            }
            GameError::TradeRejected(player1, player2, location, reason) => {
                write!(
                    f,
                    "Trade between {:?} and {:?} for {} was rejected: {}",
                    player1, player2, location, reason
                )
            }
            GameError::InsufficientResources => {
                write!(f, "Insufficient resources to make upgrade.")
            }
        }
    }
}

// get the error kind from the error
impl GameError {
    pub fn kind(&self) -> GameErrorKind {
        match self {
            GameError::PlayerNotFound(_) => GameErrorKind::PlayerNotFound,
            GameError::CellNotFound(_) => GameErrorKind::CellNotFound,
            GameError::OwnershipRequired => GameErrorKind::OwnershipRequired,
            GameError::InvalidCellType(_) => GameErrorKind::InvalidCellType,
            GameError::PurchaseableRequired(_) => GameErrorKind::PurchaseableRequired,
            GameError::UpgradeableRequired(_) => GameErrorKind::UpgradeableRequired,
            GameError::InsufficientFunds(_, _) => GameErrorKind::InsufficientFunds,
            GameError::InvalidMove => GameErrorKind::InvalidMove,
            GameError::MaxImprovementsReached => GameErrorKind::MaxImprovementsReached,
            GameError::MinImprovementsReached => GameErrorKind::MinImprovementsReached,
            GameError::MinImprovementsRequired => GameErrorKind::MinImprovementsRequired,
            GameError::MortgageRequired => GameErrorKind::MortgageRequired,
            GameError::UnMortgageRequired => GameErrorKind::UnMortgageRequired,
            GameError::InvalidImprovementTier(_) => GameErrorKind::InvalidImprovementTier,
            GameError::FailedToExitJail => GameErrorKind::FailedToExitJail,
            GameError::NegativeTransferAmount(_) => GameErrorKind::NegativeTransferAmount,
            GameError::TradeRejected(_, _, _, _) => GameErrorKind::TradeRejected,
            GameError::InsufficientResources => GameErrorKind::InsufficientResources,
        }
    }
}
