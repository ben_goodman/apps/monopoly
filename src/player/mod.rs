use rand::SeedableRng;
use rand_distr::Distribution;
use rand_pcg::Pcg32;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use tracing::info;

use crate::{
    advisor::Advisor,
    game_assets::{
        action_parser::{Action, CardType, Location, PayAmount, Transactor},
        cell::{CellType, GameCell, Improvable},
        config_parser::SimulationConfig,
        dice::{Dice, DiceRoll},
        PlayerToken,
    },
    game_error::{GameError, GameErrorKind},
    statistics::{PlayerMetrics, TurnMetrics},
    trader::{TradeOffer, TradeService},
};

use crate::bank::Bank;

mod tests;

/// The PlayerProfile struct contains player-specific settings that affect gameplay.
/// These settings are used by the Advisor to determine player behavior.
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct PlayerProfile {
    // this is how much cash the player will keep on hand when budgeting
    pub cash_reserve_percentage: f32,
    // how much more above value a player is willing to pay for something.
    // is always greater than 1.
    pub stretch_budget_multiplier: f32,
    // how close to the actual value the player will estimate for a property during
    // trade and bid. 0 is perfect accuracy, + is overestimation, - is underestimation.
    pub evaluation_accuracy: f32,
    // how much more a property is worth to the player when trading.
    // is always greater than 1.
    pub property_trade_multiplier: f32,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Player {
    pub token: PlayerToken,
    pub loc: i32,
    pub in_jail: bool,
    pub jail_timer: i32,
    pub speed_count: i32,
    pub current_roll: Option<DiceRoll>,
    pub is_bankrupt: bool,
    pub jail_free: i32,
    pub cash_reserve_percentage: f32,
    pub stretch_budget_multiplier: f32,
    pub evaluation_accuracy: f32,
    pub property_trade_multiplier: f32,
}

impl Player {
    pub fn new(token: PlayerToken, simulation_config: &SimulationConfig, player_seed: u64) -> Self {
        let mut rng: rand_pcg::Lcg64Xsh32 = Pcg32::seed_from_u64(player_seed);
        let cash_reserve_percentage = rand_distr::Normal::new(
            simulation_config
                .model_parameters
                .cash_reserve_percentage
                .mean,
            simulation_config
                .model_parameters
                .cash_reserve_percentage
                .std_dev,
        )
        .unwrap()
        .sample(&mut rng);
        let stretch_budget_multiplier: f32 = rand_distr::Normal::new(
            simulation_config
                .model_parameters
                .stretch_budget_multiplier
                .mean,
            simulation_config
                .model_parameters
                .stretch_budget_multiplier
                .std_dev,
        )
        .unwrap()
        .sample(&mut rng)
        .abs();
        let evaluation_accuracy = rand_distr::Normal::new(
            simulation_config.model_parameters.evaluation_accuracy.mean,
            simulation_config
                .model_parameters
                .evaluation_accuracy
                .std_dev,
        )
        .unwrap()
        .sample(&mut rng);
        let property_trade_multiplier = rand_distr::Normal::new(
            simulation_config
                .model_parameters
                .property_trade_multiplier
                .mean,
            simulation_config
                .model_parameters
                .property_trade_multiplier
                .std_dev,
        )
        .unwrap()
        .sample(&mut rng)
        .abs();

        Player {
            token,
            loc: 0,
            in_jail: false,
            jail_timer: 0,
            speed_count: 0,
            current_roll: None,
            is_bankrupt: false,
            jail_free: 0,
            cash_reserve_percentage,
            stretch_budget_multiplier,
            evaluation_accuracy,
            property_trade_multiplier,
        }
    }

    pub fn get_profile(&self) -> PlayerProfile {
        PlayerProfile {
            cash_reserve_percentage: self.cash_reserve_percentage,
            stretch_budget_multiplier: self.stretch_budget_multiplier,
            evaluation_accuracy: self.evaluation_accuracy,
            property_trade_multiplier: self.property_trade_multiplier,
        }
    }

    pub fn get_metrics(&self, bank: &Bank) -> PlayerMetrics {
        PlayerMetrics {
            token: self.token,
            net_worth: bank.compute_player_net_worth(self.token).unwrap_or(0),
            balance: bank.get_player_balance(self.token).unwrap_or(0),
            location: self.loc,
            location_name: bank.game_assets.cells.get_cell_at(self.loc).unwrap().name(),
        }
    }

    pub fn execute_turn(&mut self, bank: &mut Bank, turn_metrics: &mut TurnMetrics) -> Result<TurnMetrics, (GameError, TurnMetrics)> {

        // record the player's initial state for this turn
        turn_metrics.record_player_metrics(self.get_metrics(bank));

        // skip the player's turn if they are bankrupt
        if self.is_bankrupt {
            return Ok(turn_metrics.clone());
        }

        info!("Player {:?} starts their turn", self.token);

        self.attempt_player_trade(bank).map_err(|e| (e, turn_metrics.clone()))?;


        // attempt to unmortgage properties
        self.attempt_property_unmortgage(bank).map_err(|e| (e, turn_metrics.clone()))?;

        // attempt to improve properties
        self.attempt_property_group_improvement(bank).map_err(|e| (e, turn_metrics.clone()))?;

        // start by rolling the dice for this turn
        self.roll_dice(&mut bank.game_assets.dice);

        // if the player breaks the speed limit, send them to jail and end the turn
        if self.speed_count == bank.game_assets.rules.speed_limit {
            info!(
                "Player {:?} broke the speed limit and is sent to jail",
                self.token
            );
            self.in_jail = true;
            self.loc = self.find_nearest_loc(CellType::Jail, bank);
            return Ok(turn_metrics.clone());
        }

        // if the player is in jail, attempt to exit jail
        if self.in_jail {
            if self.attempt_jail_exit(bank).is_err() {
                // skip the player's turn if they fail to exit jail
                info!("Player {:?} ends their turn", self.token);
                return Ok(turn_metrics.clone());
            } else {
                // if the player exits jail, they are only allowed to
                // move if they rolled a double.
                if !self.current_roll.as_ref().unwrap().is_double {
                    info!("Player {:?} ends their turn", self.token);
                    return Ok(turn_metrics.clone());
                } else {
                    // reset the speed count if the player rolled a double
                    self.speed_count = 0;
                    // move the player by the number of spaces rolled
                    self.translate(self.current_roll.as_ref().unwrap().total, bank).map_err(|e| (e, turn_metrics.clone()))?;
                    // execute the player's current cell and end the turn
                    self.execute_current_cell(bank).map_err(|e| (e, turn_metrics.clone()))?;
                    return Ok(turn_metrics.clone())
                }
            }
        }

        // if the player was not in jail
        // move the player by the number of spaces rolled
        self.translate(self.current_roll.as_ref().unwrap().total, bank).map_err(|e| (e, turn_metrics.clone()))?;

        // log mid-turn player metrics
        turn_metrics.record_player_metrics(self.get_metrics(bank));

        // execute the player's current cell
        self.execute_current_cell(bank).map_err(|e| (e, turn_metrics.clone()))?;

        // log end-of-turn player metrics
        turn_metrics.record_player_metrics(self.get_metrics(bank));

        // take another turn if the player rolled a double, and the player is not bankrupt and is not in jail
        // players can end their turn in jail after cell eval.
        // players who go to jail in their turn do not get another turn, even if they has rolled a double.
        if self.current_roll.as_ref().unwrap().is_double && !self.is_bankrupt && !self.in_jail {
            info!(
                "Player {:?} rolled a double, taking another turn",
                self.token
            );
            return self.execute_turn(bank, turn_metrics);
        } else {
            // reset the speed count if the player did not roll a double
            self.speed_count = 0;
            info!("Player {:?} ends their turn", self.token);
            return Ok(turn_metrics.clone());
        }
    }

    /// Rolls the dice and updates the player's state.
    /// Increments the player's speed count if the player rolls a double.
    /// If the player breaks the speed limit, they are sent to jail and their speed count is reset.
    fn roll_dice(&mut self, dice: &mut Dice) -> () {
        let new_roll = dice.roll();

        info!("Player {:?} rolled {:?}", self.token, new_roll.values);

        // if a player rolls a double and is not in jail, increment the speed count
        if new_roll.is_double && !self.in_jail {
            self.speed_count += 1;
        } else {
            self.speed_count = 0;
        }

        self.current_roll = Some(new_roll);
    }

    /// moves the player by a given number of spaces
    /// positive numbers move the player forward, negative numbers move the player backward
    fn translate(&mut self, spaces: i32, bank: &mut Bank) -> Result<(), GameError> {
        let board_length = bank.game_assets.cells.cells.len() as i32;

        let new_location = (self.loc + spaces) % board_length;

        let forward_or_backward = if spaces > 0 { "forward" } else { "backward" };
        info!(
            "Player {:?} moves {} {} spaces to {}",
            self.token,
            forward_or_backward,
            spaces.abs(),
            bank.game_assets
                .cells
                .get_cell_at(new_location)
                .unwrap()
                .name()
        );

        // determine if the player will pass go
        if (self.loc + spaces) >= board_length {
            // if the player passes go, give them the pass go bonus
            info!(
                "Player {:?} passes go and collects ${}",
                self.token, bank.game_assets.rules.pass_go_bonus
            );
            bank.add_to_balance(self.token, bank.game_assets.rules.pass_go_bonus)?;
        }

        // if the player passes go counter-clockwise,
        // then the location becomes negative
        if new_location < 0 {
            self.loc = board_length + new_location;
            info!(
                "Player {:?} lands on {}",
                self.token,
                bank.game_assets.cells.get_cell_at(self.loc).unwrap().name()
            );
        } else {
            self.loc = new_location;
        }

        Ok(())
    }

    // moves the player forward to a given location
    fn advance_to(&mut self, loc: u32, bank: &mut Bank) -> Result<(), GameError> {
        let spaces = (loc as i32) - self.loc;

        // if the player is moving past go, then 'spaces' will be negative
        if spaces < 0 {
            let spaces = spaces + bank.game_assets.cells.cells.len() as i32;
            self.translate(spaces, bank)
        } else {
            self.translate(spaces, bank)
        }
    }

    fn attempt_jail_exit(&mut self, bank: &mut Bank) -> Result<(), GameError> {
        // use the following strategies to exit jail:

        // 0. exit jail if the jail timer has expired
        if self.jail_timer == bank.game_assets.rules.jail_turns {
            info!(
                "Player {:?} exits jail after waiting {} turns",
                self.token, self.jail_timer
            );
            self.in_jail = false;
            self.jail_timer = 0;
            return Ok(());
        }

        // 1. use a get out of jail free card if the player has one
        if self.jail_free > 0 {
            info!("Player {:?} uses a get out of jail free card", self.token);
            self.jail_free -= 1;
            self.in_jail = false;
            self.jail_timer = 0;
            return Ok(());
        }

        // 2. pay the jail fine if the player can afford to
        if bank.get_player_balance(self.token)? >= bank.game_assets.rules.jail_fine {
            let fine = bank.game_assets.rules.jail_fine;
            info!("Player {:?} pays the jail fine of ${}", self.token, fine);
            bank.subtract_from_balance(
                self.token,
                bank.game_assets.rules.jail_fine,
                &"To exit Jail.",
            )?;
            self.in_jail = false;
            self.jail_timer = 0;
            return Ok(());
        }

        // 3. roll a double
        if self
            .current_roll
            .as_ref()
            .expect("Expected Some DiceRoll, found None.")
            .is_double
        {
            info!(
                "Player {:?} leaves jail after rolling {:?}",
                self.token,
                self.current_roll.as_ref().unwrap().values
            );
            self.in_jail = false;
            self.jail_timer = 0;
            return Ok(());
        }

        // failed to exit jail - skip the player's turn
        // 4. wait for the next turn
        info!(
            "Player {:?} failed to exit jail and skips a turn",
            self.token
        );
        self.jail_timer += 1;
        return Err(GameError::FailedToExitJail);
    }

    fn execute_current_cell(&mut self, bank: &mut Bank) -> Result<(), GameError> {
        let current_cell_type = bank
            .game_assets
            .cells
            .get_cell_at(self.loc)
            .unwrap()
            .cell_type();
        let current_cell = bank.game_assets.cells.get_cell_at(self.loc).unwrap();
        match current_cell_type {
            CellType::Property | CellType::Railroad | CellType::Utility => {
                // pay rent to the owner (if exists) or purchase the property
                if let Some(owner) = current_cell.as_purchaseable().unwrap().owner() {
                    let rent =
                        bank.compute_current_rent(self.loc, self.current_roll.as_ref().unwrap())?;

                    // if the player is the owner, skip
                    if owner == self.token {
                        return Ok(());
                    } else {
                        info!(
                            "Player {:?} pays rent of ${} to player {:?}",
                            self.token, rent, owner
                        );
                        bank.player_to_player_transfer(self.token, owner, rent)?;
                        Ok(())
                    }
                } else {
                    // if the property is unowned, the player can purchase it
                    let purchase_cost = current_cell.as_purchaseable().unwrap().cost();
                    let has_funds = bank.assert_sufficient_player_funds(
                        self,
                        purchase_cost,
                        Some(self.cash_reserve_percentage),
                    );
                    match has_funds {
                        Ok(_) => {
                            bank.purchase_property(self, self.loc, None)?;
                            Ok(())
                        }
                        Err(_) => Ok(()),
                    }
                }
            }
            CellType::Action => {
                // execute the action on the cell
                self.execute_player_action(
                    current_cell.as_actionable().unwrap().player_action(),
                    bank,
                )
            }
            CellType::Jail => {
                // do nothing
                Ok(())
            }
        }
    }

    fn execute_player_action(
        &mut self,
        player_action: Action,
        bank: &mut Bank,
    ) -> Result<(), GameError> {
        match player_action {
            Action::Pay(pay_action) => {
                let amount = match pay_action.amount {
                    PayAmount::Number(amount) => amount,
                    PayAmount::PropertyMultiplier(house_amount, hotel_amount) => {
                        self.compute_property_multiplier(house_amount, hotel_amount, bank)
                    }
                };
                match pay_action.to {
                    Transactor::Bank => {
                        info!("Player {:?} pays ${} to the bank", self.token, amount);
                        bank.subtract_from_balance(self.token, amount, &"Player action.")
                    }
                    Transactor::AllPlayers => {
                        info!(
                            "Player {:?} pays ${} to all other players",
                            self.token, amount
                        );
                        let all_tokens: Vec<PlayerToken> = bank.get_player_tokens();
                        for receiver_token in all_tokens {
                            if receiver_token == self.token {
                                continue;
                            } else {
                                bank.player_to_player_transfer(self.token, receiver_token, amount)?;
                            }
                        }
                        Ok(())
                    }
                    Transactor::Player(to) => {
                        info!(
                            "Player {:?} pays ${} to player {:?}",
                            self.token, amount, to
                        );
                        bank.player_to_player_transfer(self.token, to, amount)
                    }
                }
            }

            Action::Receive(receive_action) => match receive_action.from {
                Transactor::Bank => {
                    info!(
                        "Player {:?} receives ${} from the bank",
                        self.token, receive_action.amount
                    );
                    bank.add_to_balance(self.token, receive_action.amount)
                }
                Transactor::AllPlayers => {
                    info!(
                        "Player {:?} receives ${} from all other players",
                        self.token, receive_action.amount
                    );
                    let all_tokens: Vec<PlayerToken> = bank.get_player_tokens();
                    for sender_token in all_tokens {
                        if sender_token == self.token {
                            continue;
                        } else {
                            bank.player_to_player_transfer(
                                sender_token,
                                self.token,
                                receive_action.amount,
                            )?;
                        }
                    }
                    Ok(())
                }
                Transactor::Player(from) => {
                    info!(
                        "Player {:?} receives ${} from player {:?}",
                        self.token, receive_action.amount, from
                    );
                    bank.player_to_player_transfer(from, self.token, receive_action.amount)
                }
            },

            Action::Translate(translate_action) => self.translate(translate_action.step, bank),

            Action::AdvanceTo(advance_action) => match advance_action.loc {
                Location::Index(loc) => self.advance_to(loc, bank),
                Location::Type(cell_type) => {
                    info!(
                        "Player {:?} advances to the nearest {:?}",
                        self.token, cell_type
                    );
                    let loc = self.find_nearest_loc(cell_type, bank);
                    self.advance_to(loc as u32, bank)
                }
            },

            Action::SetJail(set_jail_action) => {
                if set_jail_action {
                    info!("Player {:?} goes to jail", self.token);
                    self.in_jail = true;
                    self.loc = self.find_nearest_loc(CellType::Jail, bank);
                } else {
                    self.in_jail = false;
                }
                Ok(())
            }

            Action::DrawCard(card_type) => {
                let card = match card_type {
                    CardType::Chance => bank.game_assets.chance_deck.draw_card(),
                    CardType::CommunityChest => bank.game_assets.community_chest_deck.draw_card(),
                };

                let lbl = match card_type {
                    CardType::Chance => "Chance",
                    CardType::CommunityChest => "Community Chest",
                };
                info!(
                    "Player {:?} drew a {} card: {:?}",
                    self.token, lbl, card.text
                );
                self.execute_player_action(card.player_action, bank)
            }

            Action::AddJailFree => {
                info!(
                    "Player {:?} receives a get out of jail free card",
                    self.token
                );
                self.jail_free += 1;
                Ok(())
            }

            Action::Noop => {
                // do nothing
                info!("Player {:?} does nothing", self.token);
                Ok(())
            }
        }
    }

    // find the first instance of a given CellType clockwise from the player's current location
    fn find_nearest_loc(&self, cell_type: CellType, bank: &Bank) -> i32 {
        // iterate through the cells, starting from the player's location
        let mut loc = self.loc;
        let board_length = bank.game_assets.cells.cells.len() as u32;
        let mut iterations = 0;
        loop {
            iterations += 1;
            if iterations > board_length {
                panic!("Could not find cell of type {:?}", cell_type);
            }
            loc = (loc + 1) % board_length as i32;
            if bank.game_assets.cells.get_cell_at(loc).unwrap().cell_type() == cell_type {
                return loc;
            }
        }
    }

    fn compute_property_multiplier(
        &self,
        house_amount: i32,
        hotel_amount: i32,
        bank: &Bank,
    ) -> i32 {
        // determine the multiplier for rent based on the number of houses and hotels
        // improvement_tier is the number of houses or hotels on the property
        // houses are improvements 1-4, hotels are improvement 5
        // if there are no houses or hotels, return 0

        let mut house_count = 0;
        let mut hotel_count = 0;

        // get the properties owned by the player
        let purchased_properties = bank.game_assets.cells.get_cells_by_owner(&self.token);

        // filter for PropertyCell types
        let street_properties = purchased_properties
            .iter()
            .map(|cell| return cell.as_upgradeable())
            .filter(|cell| cell.is_some())
            .map(|cell| cell.unwrap());

        // count the number of houses and hotels
        for property in street_properties {
            let improvement_tier = property.get_improvement_tier();
            if improvement_tier < 5 {
                house_count += improvement_tier;
            } else {
                hotel_count += 1;
            }
        }

        // multiply house_count by the house_amount and hotel_count by the hotel_amount
        return house_count * house_amount + hotel_count * hotel_amount;
    }

    fn attempt_property_group_improvement(&self, bank: &mut Bank) -> Result<(), GameError> {
        // which property groups does the player have a monopoly on
        // and can afford to improve?

        // get all improvable properties
        let improvable_properties = bank
            .game_assets
            .cells
            .get_cells_by_type(&CellType::Property);

        // group the properties by color group.
        let mut properties_group_map: HashMap<String, Vec<&dyn GameCell>> = HashMap::new();

        for property in improvable_properties {
            let group_color = property.as_upgradeable().unwrap().get_color_group();

            if properties_group_map.contains_key(&group_color) {
                properties_group_map
                    .get_mut(&group_color)
                    .unwrap()
                    .push(property);
            } else {
                properties_group_map.insert(group_color.to_string(), vec![property]);
            }
        }

        let mut group_size_map: HashMap<String, usize> = HashMap::new();

        for (color_group, properties) in properties_group_map {
            group_size_map.insert(color_group, properties.len());
        }

        // // get properties owned by the player
        let owned_properties = bank.game_assets.cells.get_cells_by_owner_mut(&self.token);
        // get the properties that are upgradeable
        let owned_upgradeable_properties = owned_properties
            .iter()
            .map(|cell| return cell.as_upgradeable())
            .filter(|cell| cell.is_some())
            .map(|cell| cell.unwrap());

        // group the upgradeable properties by color group
        let mut player_owned_by_group: HashMap<String, Vec<&dyn Improvable>> = HashMap::new();

        for property in owned_upgradeable_properties {
            let group_color = property.as_upgradeable().unwrap().get_color_group();

            if player_owned_by_group.contains_key(&group_color) {
                player_owned_by_group
                    .get_mut(&group_color)
                    .unwrap()
                    .push(property);
            } else {
                player_owned_by_group.insert(group_color.to_string(), vec![property]);
            }
        }

        let player_owned_by_group_size: HashMap<String, usize> = player_owned_by_group
            .iter()
            .map(|(k, v)| (k.to_string(), v.len()))
            .collect();

        // compare the number of properties owned in each group to the number of properties in the group
        let player_owned_monopoly_groups = player_owned_by_group_size
            .iter()
            .filter(|(k, v)| **v == *group_size_map.get(k.as_str()).unwrap())
            .map(|(k, _v)| k.to_string())
            .collect::<Vec<String>>();

        let upgrade_eligible_groups = player_owned_monopoly_groups
            .iter()
            .filter(|group| {
                let properties = player_owned_by_group.get(*group).unwrap();
                properties
                    .iter()
                    .any(|property| property.get_improvement_tier() < 5)
            })
            .map(|x| x.to_string())
            .collect::<Vec<String>>();

        // calculate the cost to improve each property in a group
        let mut cost_to_improve: HashMap<String, i32> = HashMap::new();

        for group in upgrade_eligible_groups {
            let properties = player_owned_by_group.get(&group).unwrap();

            let mut total_cost = 0;
            for property in properties {
                let improvement_cost = property.as_upgradeable().unwrap().next_improvement_cost();
                total_cost += improvement_cost;
            }

            cost_to_improve.insert(group, total_cost);
        }

        // filter out the groups that the player can afford to improve
        // 'affordable' means that the player has enough money to improve all properties in the group
        // while maintaining a minimum cash reserve of 30% of the player's current balance.
        let affordable_groups = cost_to_improve
            .iter()
            .filter(|(_k, v)| {
                bank.assert_sufficient_player_funds(self, **v, Some(self.cash_reserve_percentage))
                    .is_ok()
            })
            .map(|(k, _v)| k.to_string())
            .collect::<Vec<String>>();

        if affordable_groups.len() == 0 {
            return Ok(());
        }

        // upgrade the first property and recurse
        let group = affordable_groups.get(0).unwrap();
        match bank.improve_property_group(self.token, group) {
            Err(e) => {
                if e.kind() == GameErrorKind::InsufficientResources {
                    Ok(())
                } else {
                    Err(e)
                }
            }
            Ok(_) => {
                info!(
                    "Player {:?} purchased improvements for the property group: {}",
                    self.token, group
                );
                self.attempt_property_group_improvement(bank)
            }
        }
    }

    fn attempt_property_unmortgage(&self, bank: &mut Bank) -> Result<(), GameError> {
        // get all properties owned by the player
        let owned_properties = bank.game_assets.cells.get_cells_by_owner_mut(&self.token);

        // filter for properties that are mortgaged
        let mut mortgaged_properties = owned_properties
            .iter()
            .filter(|property| property.get_mortgage());

        // if there are no mortgaged properties, return Ok(())
        if mortgaged_properties.clone().count() == 0 {
            return Ok(());
        }

        // attempt to unmortgage the first property
        let property = mortgaged_properties.next().unwrap();
        let unmortgage_cost = property
            .as_purchaseable()
            .expect("Expected a purchaseable property")
            .mortgage_value();
        let unmortgage_cost = unmortgage_cost + (unmortgage_cost / 10);

        let location = property.loc().clone();
        let property_name = bank.game_assets.cells.get_cell_at(location).unwrap().name();
        let has_funds = bank.assert_sufficient_player_funds(
            self,
            unmortgage_cost,
            Some(self.cash_reserve_percentage),
        );

        match has_funds {
            Ok(_) => {
                bank.unmortgage_property(self.token, location)?;
                info!(
                    "Player {:?} unmortgaged property: {}",
                    self.token, property_name
                );
                return self.attempt_property_unmortgage(bank);
            }
            Err(_) => return Ok(()),
        }
    }

    fn attempt_player_trade(&self, bank: &mut Bank) -> Result<bool, GameError> {
        // get recommended trades from the advisor
        let trades = Advisor::recommend_player_trades(
            &self,
            bank,
            self.evaluation_accuracy,
            self.property_trade_multiplier,
        )?;

        // if there are no recommended trades, return Ok(())
        if trades.len() == 0 {
            return Ok(false);
        }

        // attempt to make the first trade
        let trade = trades.get(0).unwrap();
        let (loc, cash_value) = trade;
        let property_name = bank.game_assets.cells.get_cell_at(*loc).unwrap().name();

        // get the property and property owner at the location
        let property = bank
            .game_assets
            .cells
            .get_cell_at(loc.clone())
            .unwrap()
            .as_purchaseable()
            .unwrap();
        
        let property_location = property.loc().clone();
        let property_owner = bank.players.get(&property.owner().unwrap()).unwrap().clone();

        let seller_gives = TradeOffer {
            cash: None,
            property_locations: Some(vec![property_location]),
        };

        let buyer_gives = TradeOffer {
            cash: Some(*cash_value as i32),
            property_locations: None,
        };

        // make the trade
        // if the trade is rejected, the player should
        // evaluate the counter offer and decide whether to accept or reject it.
        let trade_result = TradeService::mediate_player_trade(
            &self,
            &property_owner,
            &buyer_gives,
            &seller_gives,
            bank,
        );

        match trade_result {
            Ok(_) => {
                TradeService::resolve_trade(
                    &self,
                    &property_owner,
                    buyer_gives,
                    seller_gives,
                    bank,
                )?;
                info!(
                    "Player {:?} traded player {:?} for {}",
                    self.token, property_owner, property_name
                );
                return Ok(true);
            }
            Err((buyer_gives, seller_gives)) => {
                let buyer_gives_value = buyer_gives.appraise(
                    self,
                    bank
                ) as f32;
                let seller_gives_value = seller_gives.appraise(
                    self,
                    bank,
                ) as f32;

                // let (min, max) = (buyer_gives_value / self.stretch_budget_multiplier , buyer_gives_value * self.stretch_budget_multiplier);

                // are the two offers equivalent in value?


                // heavily favor trades with properties on each side
                let mutual_exchange = buyer_gives.property_locations.is_some()
                    && seller_gives.property_locations.is_some();

                let min_acceptable_value = if mutual_exchange {
                    seller_gives_value - (seller_gives_value / self.stretch_budget_multiplier)
                } else {
                    seller_gives_value
                };

                // println!("buyer_gives_value: {}, seller_gives_value: {}", buyer_gives_value, seller_gives_value);
                // println!("mutual exchange: {}", mutual_exchange);
                // println!("min_acceptable_value: {}", min_acceptable_value);

                // if the counter_offer value is within the min/max range of the original offer value, accept the trade
                if (buyer_gives_value <= seller_gives_value) || (mutual_exchange && seller_gives_value >= min_acceptable_value) {
                    info!(
                        "Players {:?} and {:?} agree to the trade: {:?} for {:?}",
                        self.token, property_owner.token, buyer_gives, seller_gives
                    );
                    TradeService::resolve_trade(
                        &self,
                        &property_owner,
                        buyer_gives,
                        seller_gives,
                        bank,
                    )?;
                    return Ok(true);
                } else {
                    info!(
                        "Player {:?} rejected counter-offer from player {:?}",
                        self.token, property_owner.token
                    );
                    return Ok(false);
                }
            }
        }
    }
}
