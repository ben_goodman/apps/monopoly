#[cfg(test)]

mod tests {
    use std::collections::HashMap;
    use tracing::Level;
    use tracing_subscriber::FmtSubscriber;

    use crate::{
        bank::Bank,
        game_assets::{
            action_parser::{
                Action, AdvanceToAction, CardType, Location, PayAction, PayAmount, ReceiveAction,
                Transactor,
            },
            cell::{CellType, GameCell},
            config_parser::GameConfig,
            GameAssets, PlayerToken,
        },
        player::{Player, PlayerProfile},
    };

    macro_rules! setup_test {
        () => {{
            // let subscriber = FmtSubscriber::builder()
            //     .with_max_level(Level::TRACE)
            //     .finish();

            // tracing::subscriber::set_global_default(subscriber)
            //     .expect("setting default subscriber failed");

            let config_yaml = std::fs::read_to_string(std::path::PathBuf::from("config.yaml"))
                .expect("Failed to read config.yaml");

            let game_config = GameConfig::from_yaml(&config_yaml);
            let simulation_config = game_config.simulation.clone();
            let players_config = game_config.players.clone();
            let game_assets = GameAssets::new(game_config, Some(0));

            // create a vector of players to loop through
            let players = players_config
                .iter()
                .map(|player_config| {
                    Player::new(player_config.token, &simulation_config, player_config.seed)
                })
                .collect::<Vec<Player>>();

            // let players_clone = players.clone();
            let game_assets = game_assets;
            let bank = Bank::new(players.clone(), game_assets);

            (players.clone(), bank)
        }};
    }

    #[test]
    fn test_player_new() {
        let (players, bank) = setup_test!();

        assert_eq!(players[0].token, PlayerToken::Battleship);
        assert_eq!(
            bank.get_player_balance(players[0].token)
                .expect("failed to get player bal."),
            1500
        );
        assert_eq!(players[0].loc, 0);
    }

    #[test]
    fn test_player_translate() {
        let (mut players, mut bank) = setup_test!();

        // move from GO
        assert_eq!(players[0].loc, 0);
        players[0].translate(5, &mut bank).unwrap();
        assert_eq!(players[0].loc, 5);

        // move backwards past go
        players[0].translate(-10, &mut bank).unwrap();
        assert_eq!(players[0].loc, 35);

        // move to go and collect 200
        assert_eq!(
            bank.get_player_balance(players[0].token)
                .expect("failed to get player bal."),
            1500
        );
        players[0].translate(5, &mut bank).unwrap();
        assert_eq!(players[0].loc, 0);
        assert_eq!(
            bank.get_player_balance(players[0].token)
                .expect("failed to get player bal."),
            1700
        );
    }

    #[test]
    fn test_player_roll() {
        let (mut players, mut bank) = setup_test!();

        // player rolls a [1,1]
        players[0].roll_dice(&mut bank.game_assets.dice);
        assert_eq!(players[0].current_roll.as_ref().unwrap().total, 2);
        assert_eq!(players[0].speed_count, 1);
    }

    #[test]
    fn test_find_nearest() {
        let (players, mut bank) = setup_test!();
        assert_eq!(
            players[0].find_nearest_loc(CellType::Railroad, &mut bank),
            5
        );
    }

    #[test]
    fn test_get_property_multiplier() {
        let (players, mut bank) = setup_test!();

        bank.purchase_property(&players[0], 1, None)
            .expect("Failed to purchase cell 1");
        bank.purchase_property(&players[0], 3, None)
            .expect("Failed to purchase cell 3");

        // no improvements
        assert_eq!(players[0].compute_property_multiplier(1, 2, &bank), 0);

        // 1 house on each
        let color_group = bank
            .game_assets
            .cells
            .get_cell_at(1)
            .unwrap()
            .as_upgradeable()
            .unwrap()
            .get_color_group();
        bank.improve_property_group(players[0].token, &color_group)
            .expect("Failed to improve color group");

        assert_eq!(players[0].compute_property_multiplier(1, 2, &bank), 2);

        // 1 hotel + 1 house
        bank.improve_property_group(players[0].token, &color_group)
            .expect("Failed to improve color group");
        bank.improve_property_group(players[0].token, &color_group)
            .expect("Failed to improve color group");
        bank.improve_property_group(players[0].token, &color_group)
            .expect("Failed to improve color group");
        bank.improve_property_group(players[0].token, &color_group)
            .expect("Failed to improve color group");

        assert_eq!(players[0].compute_property_multiplier(1, 2, &bank), 4);
    }

    #[test]
    fn test_player_action_pay_bank() {
        let (mut players, mut bank) = setup_test!();

        let player_action = Action::Pay(PayAction {
            amount: PayAmount::Number(100),
            to: Transactor::Bank,
        });

        let player_balance = bank
            .get_player_balance(players[0].token)
            .expect("failed to get player bal.");
        players[0]
            .execute_player_action(player_action, &mut bank)
            .expect("Failed to execute player action");
        assert_eq!(
            bank.get_player_balance(players[0].token)
                .expect("failed to get player bal."),
            player_balance - 100
        );
    }

    #[test]
    fn test_player_action_pay_property_multiplier() {
        let (mut players, mut bank) = setup_test!();

        bank.purchase_property(&players[0], 1, None)
            .expect("Failed to purchase cell 1");
        bank.purchase_property(&players[0], 3, None)
            .expect("Failed to purchase cell 3");

        bank.improve_property_group(players[0].token, "brown")
            .expect("Failed to improve color group");

        // test property multiplier
        let player_action = Action::Pay(PayAction {
            amount: PayAmount::PropertyMultiplier(10, 10),
            to: Transactor::Bank,
        });

        let player_balance = bank
            .get_player_balance(players[0].token)
            .expect("failed to get player bal.");

        players[0]
            .execute_player_action(player_action, &mut bank)
            .expect("Failed to execute player action");

        assert_eq!(
            bank.get_player_balance(players[0].token)
                .expect("failed to get player bal."),
            player_balance - 20
        );
    }

    #[test]
    fn test_player_action_pay_player() {
        let (mut players, mut bank) = setup_test!();

        let player_action = Action::Pay(PayAction {
            amount: PayAmount::Number(100),
            to: Transactor::Player(players[1].token),
        });

        let player_1_balance = bank
            .get_player_balance(players[0].token)
            .expect("failed to get player bal.");
        let player_2_balance = bank
            .get_player_balance(players[1].token)
            .expect("failed to get player bal.");

        players[0]
            .execute_player_action(player_action, &mut bank)
            .expect("Failed to execute player action");

        assert_eq!(
            bank.get_player_balance(players[0].token)
                .expect("failed to get player bal."),
            player_1_balance - 100
        );
        assert_eq!(
            bank.get_player_balance(players[1].token)
                .expect("failed to get player bal."),
            player_2_balance + 100
        );
    }

    #[test]
    fn test_player_action_pay_all_players() {
        let (mut players, mut bank) = setup_test!();

        let player_action = Action::Pay(PayAction {
            amount: PayAmount::Number(100),
            to: Transactor::AllPlayers,
        });

        let player_1_balance = bank
            .get_player_balance(players[0].token)
            .expect("failed to get player bal.");
        let player_2_balance = bank
            .get_player_balance(players[1].token)
            .expect("failed to get player bal.");
        let player_3_balance = bank
            .get_player_balance(players[2].token)
            .expect("failed to get player bal.");

        players[0]
            .execute_player_action(player_action, &mut bank)
            .expect("Failed to execute player action");

        assert_eq!(
            bank.get_player_balance(players[0].token)
                .expect("failed to get player bal."),
            player_1_balance - 400
        );
        assert_eq!(
            bank.get_player_balance(players[1].token)
                .expect("failed to get player bal."),
            player_2_balance + 100
        );
        assert_eq!(
            bank.get_player_balance(players[2].token)
                .expect("failed to get player bal."),
            player_3_balance + 100
        );
    }

    #[test]
    fn test_player_action_receive_bank() {
        let (mut players, mut bank) = setup_test!();

        let player_action = Action::Receive(ReceiveAction {
            amount: 100,
            from: Transactor::Bank,
        });

        let player_balance = bank
            .get_player_balance(players[0].token)
            .expect("failed to get player bal.");
        players[0]
            .execute_player_action(player_action, &mut bank)
            .expect("Failed to execute player action");
        assert_eq!(
            bank.get_player_balance(players[0].token)
                .expect("failed to get player bal."),
            player_balance + 100
        );
    }

    #[test]
    fn test_player_action_receive_player() {
        let (mut players, mut bank) = setup_test!();

        let player_action = Action::Receive(ReceiveAction {
            amount: 100,
            from: Transactor::Player(players[1].token),
        });

        let player_1_balance = bank
            .get_player_balance(players[0].token)
            .expect("failed to get player bal.");
        let player_2_balance = bank
            .get_player_balance(players[1].token)
            .expect("failed to get player bal.");
        players[0]
            .execute_player_action(player_action, &mut bank)
            .expect("Failed to execute player action");

        assert_eq!(
            bank.get_player_balance(players[0].token)
                .expect("failed to get player bal."),
            player_1_balance + 100
        );
        assert_eq!(
            bank.get_player_balance(players[1].token)
                .expect("failed to get player bal."),
            player_2_balance - 100
        );
    }

    #[test]
    fn test_player_action_receive_all_players() {
        let (mut players, mut bank) = setup_test!();

        let player_action = Action::Receive(ReceiveAction {
            amount: 100,
            from: Transactor::AllPlayers,
        });

        let player_1_balance = bank
            .get_player_balance(players[0].token)
            .expect("failed to get player bal.");
        let player_2_balance = bank
            .get_player_balance(players[1].token)
            .expect("failed to get player bal.");
        let player_3_balance = bank
            .get_player_balance(players[2].token)
            .expect("failed to get player bal.");

        players[0]
            .execute_player_action(player_action, &mut bank)
            .expect("Failed to execute player action");

        assert_eq!(
            bank.get_player_balance(players[0].token)
                .expect("failed to get player bal."),
            player_1_balance + 400
        );
        assert_eq!(
            bank.get_player_balance(players[1].token)
                .expect("failed to get player bal."),
            player_2_balance - 100
        );
        assert_eq!(
            bank.get_player_balance(players[2].token)
                .expect("failed to get player bal."),
            player_3_balance - 100
        );
    }

    #[test]
    fn test_player_action_advance_to_loc() {
        let (mut players, mut bank) = setup_test!();

        let player_action = Action::AdvanceTo(AdvanceToAction {
            loc: Location::Index(5),
        });

        players[0]
            .execute_player_action(player_action, &mut bank)
            .expect("Failed to execute player action");

        assert_eq!(players[0].loc, 5);
    }

    #[test]
    fn test_player_action_advance_to_type() {
        let (mut players, mut bank) = setup_test!();

        // nearest railroad is at 5
        let player_action = Action::AdvanceTo(AdvanceToAction {
            loc: Location::Type(CellType::Railroad),
        });

        players[0]
            .execute_player_action(player_action, &mut bank)
            .expect("Failed to execute player action");

        assert_eq!(players[0].loc, 5);

        // next nearest railroad is at 15
        let player_action = Action::AdvanceTo(AdvanceToAction {
            loc: Location::Type(CellType::Railroad),
        });

        players[0]
            .execute_player_action(player_action, &mut bank)
            .expect("Failed to execute player action");

        assert_eq!(players[0].loc, 15);

        // advance to next nearest utility
        let player_action = Action::AdvanceTo(AdvanceToAction {
            loc: Location::Type(CellType::Utility),
        });

        players[0]
            .execute_player_action(player_action, &mut bank)
            .expect("Failed to execute player action");

        assert_eq!(players[0].loc, 28);

        // advance to nearest property
        let player_action = Action::AdvanceTo(AdvanceToAction {
            loc: Location::Type(CellType::Property),
        });

        players[0]
            .execute_player_action(player_action, &mut bank)
            .expect("Failed to execute player action");

        assert_eq!(players[0].loc, 29);

        // advance to nearest action cell
        let player_action = Action::AdvanceTo(AdvanceToAction {
            loc: Location::Type(CellType::Action),
        });

        players[0]
            .execute_player_action(player_action, &mut bank)
            .expect("Failed to execute player action");

        assert_eq!(players[0].loc, 30);

        // advance to nearest jail
        let player_action = Action::AdvanceTo(AdvanceToAction {
            loc: Location::Type(CellType::Jail),
        });

        players[0]
            .execute_player_action(player_action, &mut bank)
            .expect("Failed to execute player action");

        assert_eq!(players[0].loc, 10);
    }

    #[test]
    fn test_player_action_set_jail() {
        let (mut players, mut bank) = setup_test!();

        // set_jail(true)
        let player_action = Action::SetJail(true);
        players[0]
            .execute_player_action(player_action, &mut bank)
            .expect("Failed to execute player action");

        assert_eq!(players[0].in_jail, true);

        // set_jail(false)
        let player_action = Action::SetJail(false);
        players[0]
            .execute_player_action(player_action, &mut bank)
            .expect("Failed to execute player action");

        assert_eq!(players[0].in_jail, false);
    }

    #[test]
    fn test_player_action_draw_card() {
        let (mut players, mut bank) = setup_test!();

        let player_action = Action::DrawCard(CardType::Chance);

        // at the current seed, this is 'go back 3 spaces'
        players[0]
            .execute_player_action(player_action, &mut bank)
            .expect("Failed to execute player action");

        assert_eq!(players[0].loc, 37);

        let player_action = Action::DrawCard(CardType::CommunityChest);

        // Bank error in your favor. Collect $200
        players[0]
            .execute_player_action(player_action, &mut bank)
            .expect("Failed to execute player action");

        assert_eq!(
            bank.get_player_balance(players[0].token)
                .expect("failed to get player bal."),
            1700
        );
    }

    #[test]
    fn test_player_action_add_jail_free() {
        let (mut players, mut bank) = setup_test!();

        assert_eq!(players[0].jail_free, 0);

        let player_action = Action::AddJailFree;

        players[0]
            .execute_player_action(player_action, &mut bank)
            .expect("Failed to execute player action");

        assert_eq!(players[0].jail_free, 1);
    }

    #[test]
    fn test_player_action_noop() {
        let (mut players, mut bank) = setup_test!();

        let player_action = Action::Noop;

        players[0]
            .execute_player_action(player_action, &mut bank)
            .expect("Failed to execute player action");

        assert_eq!(players[0].loc, 0);
    }

    #[test]
    fn test_attempt_property_group_improvement() {
        let (players, mut bank) = setup_test!();

        bank.purchase_property(&players[0], 1, None)
            .expect("Failed to purchase cell 1");
        bank.purchase_property(&players[0], 3, None)
            .expect("Failed to purchase cell 3");

        bank.purchase_property(&players[0], 37, None)
            .expect("Failed to purchase cell 37");

        players[0]
            .attempt_property_group_improvement(&mut bank)
            .expect("Failed to improve color groups");

        assert_eq!(
            bank.game_assets
                .cells
                .get_cell_at(1)
                .unwrap()
                .as_upgradeable()
                .unwrap()
                .get_improvement_tier(),
            5
        );
        assert_eq!(
            bank.game_assets
                .cells
                .get_cell_at(3)
                .unwrap()
                .as_upgradeable()
                .unwrap()
                .get_improvement_tier(),
            5
        );
    }

    #[test]
    fn test_attempt_jail_exit_via_timer() {
        let (mut players, mut bank) = setup_test!();

        players[0].in_jail = true;
        players[0].jail_timer = 3;
        players[0].jail_free = 0;

        players[0]
            .attempt_jail_exit(&mut bank)
            .expect("Failed to exit jail");

        assert_eq!(players[0].in_jail, false);
        assert_eq!(players[0].jail_timer, 0);
        assert_eq!(
            bank.get_player_balance(players[0].token)
                .expect("failed to get player bal."),
            1500
        );
    }

    #[test]
    fn test_attempt_jail_exit_via_card() {
        let (mut players, mut bank) = setup_test!();

        players[0].in_jail = true;
        players[0].jail_free = 1;

        players[0]
            .attempt_jail_exit(&mut bank)
            .expect("Failed to exit jail");

        assert_eq!(players[0].in_jail, false);
        assert_eq!(players[0].jail_free, 0);
        assert_eq!(
            bank.get_player_balance(players[0].token)
                .expect("failed to get player bal."),
            1500
        );
    }

    #[test]
    fn test_attempt_jail_exit_via_fine() {
        let (mut players, mut bank) = setup_test!();

        players[0].in_jail = true;
        players[0].jail_free = 0;
        assert_eq!(
            bank.get_player_balance(players[0].token)
                .expect("failed to get player bal."),
            1500
        );

        players[0]
            .attempt_jail_exit(&mut bank)
            .expect("Failed to exit jail");

        assert_eq!(players[0].in_jail, false);
        assert_eq!(players[0].jail_free, 0);
        assert_eq!(
            bank.get_player_balance(players[0].token)
                .expect("failed to get player bal."),
            1450
        );
    }

    #[test]
    fn test_attempt_jail_via_roll() {
        let (mut players, mut bank) = setup_test!();

        // player rolls a [1,1] (seed 0)
        players[0].roll_dice(&mut bank.game_assets.dice);

        players[0].in_jail = true;
        players[0].jail_timer = 0;
        players[0].jail_free = 0;
        bank.subtract_from_balance(players[0].token, 1500, &"")
            .expect("Failed to subtract from balance");

        players[0]
            .attempt_jail_exit(&mut bank)
            .expect("Failed to exit jail");

        assert_eq!(players[0].in_jail, false);
        assert_eq!(players[0].jail_timer, 0);
    }

    #[test]
    fn test_attempt_jail_failure() {
        let (mut players, mut bank) = setup_test!();

        // player rolls a [1,1] (seed 0)
        players[0].roll_dice(&mut bank.game_assets.dice);
        // [2,2]
        players[0].roll_dice(&mut bank.game_assets.dice);
        // not a double
        players[0].roll_dice(&mut bank.game_assets.dice);

        players[0].in_jail = true;
        players[0].jail_timer = 0;
        players[0].jail_free = 0;
        bank.subtract_from_balance(players[0].token, 1500, &"")
            .expect("Failed to subtract from balance");

        let res = players[0].attempt_jail_exit(&mut bank);

        assert_eq!(players[0].in_jail, true);
        assert_eq!(players[0].jail_timer, 1);
        assert_eq!(res.is_err(), true);
    }

    #[test]
    fn test_player_trade() {
        let (players, mut bank) = setup_test!();

        // player 0 purchases property 5
        bank.purchase_property(&players[0], 5, None)
            .expect("Failed to purchase property");

        // player 1 makes a trade
        let did_make_trade = players[1]
            .attempt_player_trade(&mut bank)
            .expect("Failed to trade");

        println!("did make trade {}", did_make_trade);
    }
}
