use clap::Parser;
use tracing::Level;
use tracing_subscriber::FmtSubscriber;


#[derive(Parser, Debug)]
#[command(name = "monopoly")]
#[command(version = env!("CARGO_PKG_VERSION"))]
#[command(about = "Simulate a game of monopoly.")]
struct Cli {
    config: String,

    #[arg(short, long)]
    seed: Option<u64>,

    #[arg(short, long)]
    verbose: bool,

    #[arg(short, long)]
    output: Option<String>,
}

fn main() {
    let args = Cli::parse();

    if args.verbose {
        let subscriber = FmtSubscriber::builder()
            .with_max_level(Level::TRACE)
            .finish();

        tracing::subscriber::set_global_default(subscriber).expect("setting default subscriber failed");
    }

    let config_yaml = std::fs::read_to_string(std::path::PathBuf::from(args.config))
        .expect("Failed to read config.yaml");

    let statistics = monopoly::simulate_game(config_yaml, args.seed);

    if let Some(output) = args.output {
        // get the file extension
        let extension = output.split('.').last().unwrap_or("");
        match extension {
            "json" => {
                let serialized_statistics = statistics.serialize_json();
                std::fs::write(output, serialized_statistics).expect("Failed to write statistics");
            }
            "yaml" => {
                let serialized_statistics = statistics.serialize_yaml();
                std::fs::write(output, serialized_statistics).expect("Failed to write statistics");
            }
            _ => {
                eprintln!("Unsupported file format: {}", extension);
            }
        }
    } else {
        println!("{}", statistics.serialize_yaml());
    }
}
