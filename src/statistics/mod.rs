use std::collections::HashMap;

use serde::{Deserialize, Serialize};

use crate::{game_assets::PlayerToken, player::PlayerProfile};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct BankMetrics {
    pub available_houses: i32,
    pub available_hotels: i32,
    pub property_ownership: HashMap<i32, Option<PlayerToken>>,
    pub property_improvement_level: HashMap<i32, i32>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct PlayerMetrics {
    pub token: PlayerToken,
    pub net_worth: i32,
    pub balance: i32,
    pub location: i32,
    pub location_name: String,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct TurnMetrics {
    pub turn_index: i32,
    pub player_metrics: Vec<PlayerMetrics>,
    // pub bank_metrics: Option<BankMetrics>,
}

impl TurnMetrics {
    pub fn new(turn_index: i32) -> Self {
        Self {
            turn_index,
            player_metrics: Vec::new(),
            // bank_metrics: None,
        }
    }

    /// Adds a snapshot of the player and bank state
    /// to the current turn metrics.
    pub fn record_player_metrics(
        &mut self,
        player_metrics: PlayerMetrics,
        // bank_metrics: BankMetrics,
    ) {
        let current_position = player_metrics.location;
        let last_recorded_position = self
            .player_metrics
            .last()
            .map(|metrics| metrics.location)
            .unwrap_or(-1);

        // if the player has moved, record the location name
        if current_position != last_recorded_position {
            self.player_metrics.push(player_metrics);
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct GameStatistics {
    pub random_seed: u64,
    pub turn_count: i32,
    pub player_stats: HashMap<PlayerToken, PlayerProfile>,
    pub turn_metrics: Vec<TurnMetrics>,
}

impl GameStatistics {
    pub fn new(player_stats: HashMap<PlayerToken, PlayerProfile>, random_seed: u64) -> Self {
        Self {
            turn_count: 0,
            player_stats,
            turn_metrics: Vec::new(),
            random_seed,
        }
    }

    pub fn record_turn(&mut self, turn_metrics: TurnMetrics) {
        self.turn_count += 1;
        self.turn_metrics.push(turn_metrics);
    }

    pub fn serialize_yaml(&self) -> String {
        serde_yaml::to_string(&self).unwrap()
    }

    pub fn serialize_json(&self) -> String {
        serde_json::to_string(&self).unwrap()
    }
}
