use std::ops::Mul;

use tracing::info;

use crate::{
    advisor::Advisor,
    bank::Bank,
    game_assets::{cell::GameCell, PlayerToken},
    game_error::GameError,
    player::Player,
};

mod tests;

// Trades can include cash and properties
#[derive(Debug, Clone, PartialEq)]
pub struct TradeOffer {
    pub cash: Option<i32>,
    pub property_locations: Option<Vec<i32>>,
}

impl TradeOffer {
    // properties are worth twice their cash value in a trade
    pub fn appraise(&self, player: &Player, bank: &Bank) -> i32 {
        let mut total_value = self.cash.unwrap_or(0);
        let accuracy = player.evaluation_accuracy;

        if let Some(ref locations) = self.property_locations {
            for location in locations {
                let base_property_value =
                    Advisor::estimate_property_worth(*location, player, accuracy, bank).unwrap()
                        as f32;
                total_value += base_property_value
                    .mul(player.property_trade_multiplier)
                    .floor() as i32;
            }
        }

        total_value
    }
}

impl std::fmt::Display for TradeOffer {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut s = format!("Cash: {:?} - Properties: ", self.cash);
        if let Some(ref locations) = self.property_locations {
            for loc in locations {
                s.push_str(&format!("{:?}, ", loc));
            }
        }
        write!(f, "{}", s)
    }
}

pub struct TradeService {}

impl<'a> TradeService {
    /// Mediate a trade between a buyer and a seller.
    /// Acts on behalf of the seller to determine if the trade is fair.
    pub fn mediate_player_trade(
        buyer: &Player,
        seller: &Player,
        buyer_gives: &TradeOffer,
        seller_gives: &TradeOffer,
        bank: &Bank,
    ) -> Result<(), (TradeOffer, TradeOffer)> {
        info!(
            "Player {:?} offers to trade player {:?} for {} in exchange for {}",
            buyer.token, seller.token, buyer_gives, seller_gives
        );

        // properties that the buyer has.
        let buyers_property_locations = bank
            .game_assets
            .cells
            .get_cells_by_owner(&buyer.token)
            .iter()
            .map(|cell| cell.loc())
            .collect::<Vec<i32>>();

        let seller_eval_accuracy = seller.evaluation_accuracy;
        let seller_trade_multiplier = seller.property_trade_multiplier;

        // which properties does the seller want?
        let trades_for_seller: Vec<i32> = Advisor::recommend_player_trades(
            &seller,
            bank,
            seller_eval_accuracy,
            seller_trade_multiplier,
        )
        .expect("Failed to gather trades")
        .iter()
        .map(|trade| trade.0)
        .collect();

        // properties that the seller wants that the buyer has
        // are locations that are in both lists
        let common_properties = trades_for_seller
            .iter()
            .filter(|loc| buyers_property_locations.contains(loc))
            .collect::<Vec<&i32>>();

        // println!("Common properties: {:?}", common_properties);

        if common_properties.len() > 0 {
            // always give a counter offer if the buyer has properties that the seller wants
            let counter_offer =
                TradeService::compute_counter_trade(buyer, seller, buyer_gives, seller_gives, bank);
            Err(counter_offer)
        } else {
            // if the buyer has no properties that the seller wants
            // then the seller determines if the trade is fair
            let buyer_gives_value = buyer_gives.appraise(seller, bank);
            let seller_gives_value = seller_gives.appraise(buyer, bank);

            if buyer_gives_value >= seller_gives_value {
                // agree to trade
                Ok(())
            } else {
                // the seller wants more in return
                let counter_offer = TradeService::compute_counter_trade(
                    buyer,
                    seller,
                    buyer_gives,
                    seller_gives,
                    bank,
                );

                info!(
                    "Player {:?} rejects trade offer from player {:?}. Counter offer: {:?}",
                    seller.token, buyer.token, counter_offer
                );

                // return the counter offer to the buyer
                Err(counter_offer)
            }
        }
    }

    /// Compute the seller's counter-offer to the buyer
    /// A counter offer is a tuple of the possibly updated buyer's offer and the seller's new offer
    fn compute_counter_trade(
        buyer: &Player,
        seller: &Player,
        buyer_gives: &TradeOffer,
        seller_gives: &TradeOffer,
        bank: &Bank,
    ) -> (TradeOffer, TradeOffer) {
        // player 'buyer' offers trade_offer to player 'seller'
        // does the buyer have any properties to offer?

        // properties that the buyer has.
        let buyers_property_locations = bank
            .game_assets
            .cells
            .get_cells_by_owner(&buyer.token)
            .iter()
            .map(|cell| cell.loc())
            .collect::<Vec<i32>>();

        // which properties does the seller want?
        let trades_for_seller: Vec<i32> = Advisor::recommend_player_trades(
            seller,
            bank,
            seller.evaluation_accuracy,
            seller.property_trade_multiplier,
        )
        .expect("Failed to gather trades")
        .iter()
        .map(|trade| trade.0)
        .collect();

        // properties that the seller wants that the buyer has
        // are locations that are in both lists
        let common_properties = trades_for_seller
            .iter()
            .filter(|loc| buyers_property_locations.contains(loc))
            .collect::<Vec<&i32>>();

        let mut seller_new_offer = TradeOffer {
            cash: None,
            property_locations: seller_gives.property_locations.clone(),
        };

        // if the buyer has any properties that the seller wants
        // then the buyer can offer them in the trade.
        let mut buyer_new_offer = buyer_gives.clone();

        if !common_properties.is_empty() {
            // println!("Common properties: {:?}", common_properties);

            // add the first common property to the counter offer
            buyer_new_offer.property_locations = Some(vec![*common_properties[0]]);

            // recompute the cash difference between the two offers
            let buyer_gives_value = buyer_new_offer.appraise(seller, bank);
            let seller_gives_value = seller_gives.appraise(buyer, bank);

            // println!("buyer_gives_value: {}, seller_gives_value: {}", buyer_gives_value, seller_gives_value);

            let cash_difference = seller_gives_value - buyer_gives_value;

            if cash_difference < 0 {
                // if the cash difference is -ve, then the buyer pays the difference
                buyer_new_offer.cash = Some(buyer_gives.cash.unwrap_or(0) + cash_difference.abs() );

                let seller_cash_diff = seller_gives.cash.unwrap_or(0) - cash_difference.abs();
                if seller_cash_diff < 0 {
                    seller_new_offer.cash = None;
                } else {
                    seller_new_offer.cash = Some(seller_cash_diff);
                }
            } else {
                // the seller will pay the difference
                seller_new_offer.cash = Some(seller_gives.cash.unwrap_or(0) + cash_difference);
            }

        } else {
            // the buyer has no properties that the seller wants
            // so the counter offer will be cash only

            // the buyer will pay the difference
            let cash_difference = buyer_gives.appraise(seller, bank)
                - seller_gives.appraise(buyer, bank);
            buyer_new_offer.cash = Some(buyer_gives.cash.unwrap_or(0) + cash_difference.abs());
        }

        info!(
            "Player {:?} counters with a new offer: {} <=> {}",
            seller.token, buyer_new_offer, seller_new_offer
        );

        (buyer_new_offer, seller_new_offer)
        // counter_offer
    }

    pub fn resolve_trade(
        buyer: &Player,
        seller: &Player,
        buyer_gives: TradeOffer,
        seller_gives: TradeOffer,
        bank: &mut Bank,
    ) -> Result<(), GameError> {
        if let Some(locations) = buyer_gives.property_locations {
            info!(
                "Player {:?} trades properties {:?} to player {:?}",
                buyer.token, locations, seller.token
            );
            for loc in locations {
                bank.game_assets
                    .cells
                    .get_cell_at_mut(loc)
                    .unwrap()
                    .as_purchaseable()
                    .unwrap()
                    .owner()
                    .replace(seller.token);
            }
        }

        if let Some(locations) = seller_gives.property_locations {
            info!(
                "Player {:?} trades properties {:?} to player {:?}",
                seller.token, locations, buyer.token
            );
            for loc in locations {
                bank.game_assets
                    .cells
                    .get_cell_at_mut(loc)
                    .unwrap()
                    .as_purchaseable()
                    .unwrap()
                    .owner()
                    .replace(buyer.token);
            }
        }

        bank.player_to_player_transfer(buyer.token, seller.token, buyer_gives.cash.unwrap_or(0))?;
        bank.player_to_player_transfer(seller.token, buyer.token, seller_gives.cash.unwrap_or(0))?;

        Ok(())
    }
}
