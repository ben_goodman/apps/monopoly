#[cfg(test)]

mod tests {
    use crate::bank::Bank;
    use crate::game_assets::config_parser::GameConfig;
    use crate::game_assets::GameAssets;
    use crate::player::Player;
    use crate::trader::TradeOffer;
    use crate::trader::TradeService;

    // use tracing::Level;
    // use tracing_subscriber::FmtSubscriber;

    macro_rules! setup_test {
        () => {{
            // let subscriber = FmtSubscriber::builder()
            //     .with_max_level(Level::TRACE)
            //     .finish();

            // tracing::subscriber::set_global_default(subscriber)
            //     .expect("setting default subscriber failed");

            let config_yaml = std::fs::read_to_string(std::path::PathBuf::from("config.yaml"))
                .expect("Failed to read config.yaml");

            let game_config = GameConfig::from_yaml(&config_yaml);
            let simulation_config = game_config.simulation.clone();
            let players_config = game_config.players.clone();
            let game_assets = GameAssets::new(game_config, Some(0));

            // create a vector of players to loop through
            let players = players_config
                .iter()
                .map(|player_config| {
                    Player::new(player_config.token, &simulation_config, player_config.seed)
                })
                .collect::<Vec<Player>>();

            // let players_clone = players.clone();
            let game_assets = game_assets;
            let bank = Bank::new(players.clone(), game_assets);

            (players.clone(), bank)
        }};
    }

    #[test]
    fn test_player_mediate_trade_accept() {
        let (players, mut bank) = setup_test!();

        let buyer = &players[0];
        let seller = &players[1];

        // player 1 purchases property 1
        bank.purchase_property(seller, 1, None)
            .expect("Failed to purchase property");

        // player 0 makes an offer
        // will give $80
        let buyer_gives = TradeOffer {
            cash: Some(80),
            property_locations: None,
        };

        // in exchange for property 1
        let seller_gives = TradeOffer {
            cash: None,
            property_locations: Some(vec![1]),
        };

        let result =
            TradeService::mediate_player_trade(buyer, seller, &buyer_gives, &seller_gives, &bank);

        // let counter_offer = TradeOffer {
        //     cash: Some(113),
        //     property_locations: None,
        // };

        assert_eq!(result, Ok(()));
    }

    #[test]
    fn test_player_mediate_trade_reject() {
        let (players, mut bank) = setup_test!();

        let buyer = &players[0];
        let seller = &players[1];

        // player 1 purchases property 1
        bank.purchase_property(seller, 1, None)
            .expect("Failed to purchase property");

        // player 0 purchases property 5
        bank.purchase_property(buyer, 5, None)
            .expect("Failed to purchase property");

        // player 0 makes an offer
        let buyer_gives = TradeOffer {
            cash: Some(30),
            property_locations: None,
        };

        // in exchange for property 1
        let seller_gives = TradeOffer {
            cash: None,
            property_locations: Some(vec![1]),
        };

        let result =
            TradeService::mediate_player_trade(buyer, seller, &buyer_gives, &seller_gives, &bank);

        println!("result: {:?}", result);

        // let counter_offer = TradeOffer {
        //     cash: Some(113),
        //     property_locations: None,
        // };

        // assert_eq!(result, Ok(()));
    }
}
