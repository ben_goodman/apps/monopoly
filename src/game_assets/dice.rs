use rand::{Rng, SeedableRng};
use rand_pcg::Pcg32;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize, PartialEq)]
pub struct Die {
    pub min: i32,
    pub max: i32,
}

#[derive(Clone, Debug, Deserialize, PartialEq)]
pub struct DiceRoll {
    pub values: Vec<i32>,
    pub total: i32,
    pub is_double: bool,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Dice {
    pub dice: Vec<Die>,
    pub rng: rand_pcg::Lcg64Xsh32,
}

impl Dice {
    pub fn new(dice: Vec<Die>, seed: u64) -> Self {
        let rng: rand_pcg::Lcg64Xsh32 = Pcg32::seed_from_u64(seed);
        Dice { dice, rng }
    }

    pub fn roll(&mut self) -> DiceRoll {
        // roll each die and collect the values
        let values = self
            .dice
            .clone()
            .iter()
            .map(|die| self.rng.gen_range(die.min..die.max))
            .map(|val| val as i32)
            .collect::<Vec<i32>>();

        // sum the values
        let total = values.iter().sum();

        // is each value the same?
        let first_value = values[0].clone();
        let is_double = values.iter().all(|val| *val == first_value);

        return DiceRoll {
            values,
            total,
            is_double,
        };
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_roll() {
        let mut dice = Dice::new(vec![Die { min: 1, max: 6 }, Die { min: 1, max: 6 }], 123);

        let dice_roll = dice.roll();

        assert_eq!(dice_roll.values, vec![3, 2]);
        assert!(dice_roll.total == 5);
        assert!(!dice_roll.is_double);
    }

    #[test]
    fn test_roll_doubles() {
        let mut dice = Dice::new(vec![Die { min: 1, max: 6 }, Die { min: 1, max: 6 }], 0);

        let dice_roll = dice.roll();

        assert_eq!(dice_roll.values, vec![1, 1]);
        assert!(dice_roll.total == 2);
        assert!(dice_roll.is_double);
    }
}
