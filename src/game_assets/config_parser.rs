use serde::{Deserialize, Serialize};

use super::PlayerToken;

#[derive(Clone, Debug, Deserialize, Serialize, PartialEq)]
pub struct Player {
    pub token: PlayerToken,
    pub seed: u64,
}

#[derive(Clone, Debug, Deserialize, Serialize, PartialEq)]
pub struct ModelParameterValue {
    pub mean: f32,
    pub std_dev: f32,
}

#[derive(Clone, Debug, Deserialize, Serialize, PartialEq)]
pub struct ModelParameters {
    pub cash_reserve_percentage: ModelParameterValue,
    pub stretch_budget_multiplier: ModelParameterValue,
    pub evaluation_accuracy: ModelParameterValue,
    pub property_trade_multiplier: ModelParameterValue,
}

#[derive(Clone, Debug, Deserialize, Serialize, PartialEq)]
pub struct SimulationConfig {
    pub seed: Option<u64>,
    pub max_iterations: i32,
    pub model_parameters: ModelParameters,
}



#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct GameRulesConfig {
    pub starting_balance: i32,
    pub pass_go_bonus: i32,
    pub jail_fine: i32,
    pub jail_turns: i32,
    pub speed_limit: i32,
    pub bank_free_parking: bool,
    pub enable_auctions: bool,
    pub monopoly_rent_multiplier: i32,
    pub house_count: i32,
    pub hotel_count: i32,
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct DieConfig {
    pub min: i32,
    pub max: i32,
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct ActionCardConfig {
    pub text: String,
    pub player_action: String,
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct ActionCellConfig {
    pub name: String,
    pub loc: i32,
    pub player_action: String,
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct PropertyCellConfig {
    pub name: String,
    pub loc: i32,
    pub group: String,
    pub cost: i32,
    pub rent: Vec<i32>,
    pub house_cost: i32,
    pub hotel_cost: i32,
    pub mortgage_value: i32,
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct RailroadCellConfig {
    pub name: String,
    pub loc: i32,
    pub cost: i32,
    pub rent: Vec<i32>,
    pub mortgage_value: i32,
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct UtilityCellConfig {
    pub name: String,
    pub loc: i32,
    pub cost: i32,
    pub rent: Vec<i32>,
    pub mortgage_value: i32,
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct JailCellConfig {
    pub name: String,
    pub loc: i32,
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct GameConfig {
    pub players: Vec<Player>,
    pub simulation: SimulationConfig,
    pub rules: GameRulesConfig,
    pub dice: Vec<DieConfig>,
    pub chance_deck: Vec<ActionCardConfig>,
    pub community_chest_deck: Vec<ActionCardConfig>,
    pub property_cells: Vec<PropertyCellConfig>,
    pub railroad_cells: Vec<RailroadCellConfig>,
    pub utility_cells: Vec<UtilityCellConfig>,
    pub action_cells: Vec<ActionCellConfig>,
    pub jail_cells: Vec<JailCellConfig>,
}

impl GameConfig {
    pub fn from_yaml(config: &str) -> Self {
        serde_yaml::from_str(config).expect("Failed to parse YAML")
    }
}
