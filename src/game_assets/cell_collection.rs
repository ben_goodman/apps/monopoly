use serde::Deserialize;

use crate::game_assets::cell::GameCell;
use std::collections::HashMap;

use super::{
    cell::{CellType, Improvable, Purchaseable},
    PlayerToken,
};

#[derive(Clone, Debug, Deserialize, PartialEq)]
pub struct CellCollection<T: GameCell> {
    pub cells: HashMap<i32, T>,
}

impl<T: GameCell> CellCollection<T> {
    pub fn new(cells: Vec<T>) -> Self {
        let mut cell_map = HashMap::new();
        for cell in cells {
            cell_map.insert(cell.loc(), cell);
        }

        CellCollection { cells: cell_map }
    }

    // Gets the cell at the given location
    pub fn get_cell_at(&self, loc: i32) -> Option<&T> {
        self.cells.get(&loc)
    }

    // Gets the cell at the given location, mutable
    pub fn get_cell_at_mut(&mut self, loc: i32) -> Option<&mut T> {
        self.cells.get_mut(&loc)
    }

    // gets cells as an iterator
    pub fn iter(&self) -> impl Iterator<Item = &T> {
        self.cells.values()
    }

    // pub fn iter_mut(&mut self) -> impl Iterator<Item = &mut T> {
    //     self.cells.values_mut()
    // }

    // gets cells by their type: property, railroad, etc.
    pub fn get_cells_by_type(&self, cell_type: &CellType) -> Vec<&T> {
        self.cells
            .values()
            .filter(|cell| cell.cell_type() == *cell_type)
            .collect()
    }

    // pub fn get_cells_by_type_mut(&mut self, cell_type: &CellType) -> Vec<&mut T> {
    //     self.cells
    //         .values_mut()
    //         .filter(|cell| cell.cell_type() == *cell_type)
    //         .collect()
    // }

    pub fn get_cells_by_owner(&self, owner: &PlayerToken) -> Vec<&dyn Purchaseable> {
        self.cells
            .values()
            // filter for purchaseable cells
            .filter_map(|cell| return cell.as_purchaseable())
            // filter for cells owned by the player
            .filter(|purchaseable| purchaseable.owner().as_ref() == Some(owner))
            .collect()
    }

    pub fn get_cells_by_owner_mut(&mut self, owner: &PlayerToken) -> Vec<&mut dyn Purchaseable> {
        self.cells
            .values_mut()
            // filter for purchaseable cells
            .filter_map(|cell| return cell.as_purchaseable_mut())
            // filter for cells owned by the player
            .filter(|purchaseable| purchaseable.owner().as_ref() == Some(owner))
            .collect()
    }

    pub fn get_cells_by_color_group(&self, color_group: &str) -> Vec<&dyn Improvable> {
        self.cells
            .values()
            // filter for purchaseable cells
            .filter_map(|cell| return cell.as_upgradeable())
            // filter for cells in the color group
            .filter(|purchaseable| purchaseable.get_color_group() == color_group)
            .collect()
    }

    // pub fn get_cells_by_color_group_mut(&mut self, color_group: &str) -> Vec<&mut dyn Improvable> {
    //     self.cells
    //         .values_mut()
    //         // filter for purchaseable cells
    //         .filter_map(|cell| return cell.as_upgradeable_mut())
    //         // filter for cells in the color group
    //         .filter(|purchaseable| purchaseable.get_color_group() == color_group)
    //         .collect()
    // }
}
