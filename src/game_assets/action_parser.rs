use pest::Parser;
use pest_derive::Parser;
use serde::{Deserialize, Serialize};
use std::str::FromStr;

use super::{cell::CellType, PlayerToken};

#[derive(Parser)]
#[grammar = "src/game_assets/action_grammar.pest"]
struct ActionParser;

#[derive(Clone, Debug, PartialEq)]
pub enum Action {
    Pay(PayAction),
    Receive(ReceiveAction),
    Translate(TranslateAction),
    AdvanceTo(AdvanceToAction),
    SetJail(bool),
    DrawCard(CardType),
    AddJailFree,
    Noop,
}

impl FromStr for Action {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(parse_action(s))
    }
}

#[derive(Clone, Debug, PartialEq, Deserialize, Serialize)]
pub enum CardType {
    Chance,
    CommunityChest,
}

#[derive(Clone, Debug, PartialEq, Deserialize, Serialize)]
pub enum ActionMacro {
    PropertyMultiplierMacro(u32, u32),
    NearestCellMacro(CellType),
}

#[derive(Clone, Debug, PartialEq)]
pub enum Transactor {
    Bank,
    Player(PlayerToken),
    AllPlayers,
}

#[derive(Clone, Debug, PartialEq, Deserialize, Serialize)]
pub enum PayAmount {
    Number(i32),
    PropertyMultiplier(i32, i32),
}

#[derive(Clone, Debug, PartialEq)]
pub struct PayAction {
    pub amount: PayAmount,
    pub to: Transactor,
}

#[derive(Clone, Debug, PartialEq)]
pub struct ReceiveAction {
    pub amount: i32,
    pub from: Transactor,
}

#[derive(Clone, Debug, PartialEq, Deserialize, Serialize)]
pub struct TranslateAction {
    pub step: i32,
}

#[derive(Clone, Debug, PartialEq, Deserialize, Serialize)]
pub enum Location {
    Index(u32),
    Type(CellType),
}

#[derive(Clone, Debug, PartialEq, Deserialize, Serialize)]
pub struct AdvanceToAction {
    pub loc: Location,
}

#[derive(Clone, Debug, PartialEq, Deserialize, Serialize)]
pub struct SetJailAction {
    pub in_jail: bool,
}

fn parse_action(action: &str) -> Action {
    let mut pairs = ActionParser::parse(Rule::action, action).unwrap_or_else(|e| panic!("{}", e));

    let collection = pairs.next().unwrap();

    for pair in collection.into_inner() {
        match pair.as_rule() {
            Rule::pay => {
                let mut pay_pair = pair.into_inner();

                // amount is either a number or a macro
                let pay_amount = pay_pair.next().unwrap();
                let amount = match pay_amount.as_rule() {
                    Rule::property_multiplier_macro => {
                        let mut multiplier_pair = pay_amount.into_inner();
                        let first = multiplier_pair
                            .next()
                            .unwrap()
                            .as_str()
                            .parse::<i32>()
                            .unwrap();
                        let second = multiplier_pair
                            .next()
                            .unwrap()
                            .as_str()
                            .parse::<i32>()
                            .unwrap();
                        PayAmount::PropertyMultiplier(first, second)
                    }
                    _ => PayAmount::Number(pay_amount.as_str().parse::<i32>().unwrap()),
                };

                // if the receiver is not specified, default to Bank
                let receiver = match pay_pair.next() {
                    Some(p) => {
                        match p.as_rule() {
                            Rule::to => {
                                // either BANK or all_players macro
                                let receiver = p.into_inner().next().unwrap();
                                match receiver.as_rule() {
                                    Rule::bank => Transactor::Bank,
                                    Rule::all_players => Transactor::AllPlayers,
                                    _ => {
                                        unreachable!("Unknown receiver: {:?}", receiver);
                                    }
                                }
                            }

                            _ => {
                                unreachable!("Unknown receiver: {:?}", p);
                            }
                        }
                    }
                    None => Transactor::Bank,
                };

                return Action::Pay(PayAction {
                    amount,
                    to: receiver,
                });
            }
            Rule::receive => {
                let mut receive_pair = pair.into_inner();

                let amount = receive_pair
                    .next()
                    .unwrap()
                    .as_str()
                    .parse::<i32>()
                    .unwrap();

                // if the receiver is not specified, default to Bank
                let from = match receive_pair.next() {
                    Some(p) => {
                        match p.as_rule() {
                            Rule::from => {
                                // either BANK or all_players macro
                                let receiver = p.into_inner().next().unwrap();
                                match receiver.as_rule() {
                                    Rule::bank => Transactor::Bank,
                                    Rule::all_players => Transactor::AllPlayers,
                                    _ => {
                                        unreachable!("Unknown receiver: {:?}", receiver);
                                    }
                                }
                            }

                            _ => {
                                unreachable!("Unknown receiver: {:?}", p);
                            }
                        }
                    }
                    None => Transactor::Bank,
                };

                return Action::Receive(ReceiveAction { amount, from });
            }
            Rule::advance_to => {
                // can either be a location index or a macro
                let mut advance_pair = pair.into_inner();
                let advance_to = advance_pair.next().unwrap();
                match advance_to.as_rule() {
                    Rule::cell => {
                        let loc = advance_to.as_str().parse::<u32>().ok();
                        return Action::AdvanceTo(AdvanceToAction {
                            loc: Location::Index(loc.unwrap()),
                        });
                    }
                    Rule::nearest_cell_macro => {
                        // get the location_type
                        let mut nearest_pair = advance_to.into_inner();
                        let location_type = nearest_pair.next().unwrap();
                        let loc_type = match location_type.as_str() {
                            "property" => CellType::Property,
                            "action_cell" => CellType::Action,
                            "railroad" => CellType::Railroad,
                            "utility" => CellType::Utility,
                            "jail" => CellType::Jail,
                            _ => {
                                unreachable!("Unknown location type: {:?}", location_type);
                            }
                        };
                        return Action::AdvanceTo(AdvanceToAction {
                            loc: Location::Type(loc_type),
                        });
                    }
                    _ => {
                        unreachable!("Unknown advance_to: {:?}", advance_to);
                    }
                }
            }
            Rule::draw_card => {
                // get the card type
                let card_type = pair.into_inner().next().unwrap();
                let card = match card_type.as_str() {
                    "chance" => CardType::Chance,
                    "community_chest" => CardType::CommunityChest,
                    _ => {
                        unreachable!("Unknown card type: {:?}", card_type);
                    }
                };
                return Action::DrawCard(card);
            }
            Rule::translate => {
                let step = pair
                    .into_inner()
                    .next()
                    .unwrap()
                    .as_str()
                    .parse::<i32>()
                    .unwrap();
                return Action::Translate(TranslateAction { step });
            }
            Rule::set_jail => {
                let in_jail = pair.into_inner().next().unwrap().as_str();
                match in_jail {
                    "true" => {
                        return Action::SetJail(true);
                    }
                    "false" => {
                        return Action::SetJail(false);
                    }
                    _ => {
                        unreachable!("Unknown in_jail: {:?}", in_jail);
                    }
                }
            }
            Rule::add_jail_free => {
                return Action::AddJailFree;
            }
            Rule::noop => {
                return Action::Noop;
            }
            _ => {
                unreachable!("Unknown action: {:?}", pair);
            }
        }
    }

    // Default action if no match is found
    Action::Noop
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_pay_bank() {
        let action = "PAY 100 BANK";
        let parsed_action = parse_action(action);
        assert_eq!(
            parsed_action,
            Action::Pay(PayAction {
                amount: PayAmount::Number(100),
                to: Transactor::Bank
            })
        );
    }

    #[test]
    fn test_parse_pay_bank_default() {
        let action = "PAY 100";
        let parsed_action = parse_action(action);
        assert_eq!(
            parsed_action,
            Action::Pay(PayAction {
                amount: PayAmount::Number(100),
                to: Transactor::Bank
            })
        );
    }

    #[test]
    fn test_parse_pay_all_players() {
        let action = "PAY 50 all_players";
        let parsed_action = parse_action(action);
        assert_eq!(
            parsed_action,
            Action::Pay(PayAction {
                amount: PayAmount::Number(50),
                to: Transactor::AllPlayers
            })
        );
    }

    #[test]
    fn test_parse_pay_properties_macro() {
        let action = "PAY property_multiplier(15,25)";
        let parsed_action = parse_action(action);
        assert_eq!(
            parsed_action,
            Action::Pay(PayAction {
                amount: PayAmount::PropertyMultiplier(15, 25),
                to: Transactor::Bank
            })
        );
    }

    #[test]
    fn test_parse_receive_bank_implicit() {
        let action = "RECEIVE 100";
        let parsed_action = parse_action(action);
        assert_eq!(
            parsed_action,
            Action::Receive(ReceiveAction {
                amount: 100,
                from: Transactor::Bank
            })
        );
    }

    #[test]
    fn test_parse_receive_bank() {
        let action = "RECEIVE 100 BANK";
        let parsed_action = parse_action(action);
        assert_eq!(
            parsed_action,
            Action::Receive(ReceiveAction {
                amount: 100,
                from: Transactor::Bank
            })
        );
    }

    #[test]
    fn test_parse_receive_all_players() {
        let action = "RECEIVE 50 all_players";
        let parsed_action = parse_action(action);
        assert_eq!(
            parsed_action,
            Action::Receive(ReceiveAction {
                amount: 50,
                from: Transactor::AllPlayers
            })
        );
    }

    #[test]
    fn test_parse_translate_forward() {
        let action = "TRANSLATE 3";
        let parsed_action = parse_action(action);
        assert_eq!(
            parsed_action,
            Action::Translate(TranslateAction { step: 3 })
        );
    }

    #[test]
    fn test_parse_translate_backward() {
        let action = "TRANSLATE -3";
        let parsed_action = parse_action(action);
        assert_eq!(
            parsed_action,
            Action::Translate(TranslateAction { step: -3 })
        );
    }

    #[test]
    fn test_parse_advance_to() {
        let action = "ADVANCE_TO 5";
        let parsed_action = parse_action(action);
        assert_eq!(
            parsed_action,
            Action::AdvanceTo(AdvanceToAction {
                loc: Location::Index(5)
            })
        );
    }

    #[test]
    fn test_parse_advance_to_railroad() {
        let action = "ADVANCE_TO nearest(railroad)";
        let parsed_action = parse_action(action);
        assert_eq!(
            parsed_action,
            Action::AdvanceTo(AdvanceToAction {
                loc: Location::Type(CellType::Railroad)
            })
        );
    }

    #[test]
    fn test_parse_advance_to_utility() {
        let action = "ADVANCE_TO nearest(utility)";
        let parsed_action = parse_action(action);
        assert_eq!(
            parsed_action,
            Action::AdvanceTo(AdvanceToAction {
                loc: Location::Type(CellType::Utility)
            })
        );
    }

    #[test]
    fn test_parse_advance_to_property() {
        let action = "ADVANCE_TO nearest(property)";
        let parsed_action = parse_action(action);
        assert_eq!(
            parsed_action,
            Action::AdvanceTo(AdvanceToAction {
                loc: Location::Type(CellType::Property)
            })
        );
    }

    #[test]
    fn test_parse_advance_to_jail() {
        let action = "ADVANCE_TO nearest(jail)";
        let parsed_action = parse_action(action);
        assert_eq!(
            parsed_action,
            Action::AdvanceTo(AdvanceToAction {
                loc: Location::Type(CellType::Jail)
            })
        );
    }

    #[test]
    fn test_parse_advance_to_action_cell() {
        let action = "ADVANCE_TO nearest(action_cell)";
        let parsed_action = parse_action(action);
        assert_eq!(
            parsed_action,
            Action::AdvanceTo(AdvanceToAction {
                loc: Location::Type(CellType::Action)
            })
        );
    }

    #[test]
    fn test_parse_draw_card_chance() {
        let action = "DRAW_CARD chance";
        let parsed_action = parse_action(action);
        assert_eq!(parsed_action, Action::DrawCard(CardType::Chance));
    }

    #[test]
    fn test_parse_draw_card_community_chest() {
        let action = "DRAW_CARD community_chest";
        let parsed_action = parse_action(action);
        assert_eq!(parsed_action, Action::DrawCard(CardType::CommunityChest));
    }

    #[test]
    fn test_parse_set_jail_true() {
        let action = "SET_JAIL true";
        let parsed_action = parse_action(action);
        assert_eq!(parsed_action, Action::SetJail(true));
    }

    #[test]
    fn test_parse_set_jail_false() {
        let action = "SET_JAIL false";
        let parsed_action = parse_action(action);
        assert_eq!(parsed_action, Action::SetJail(false));
    }

    #[test]
    fn test_parse_noop() {
        let action = "NOOP";
        let parsed_action = parse_action(action);
        assert_eq!(parsed_action, Action::Noop);
    }
}
