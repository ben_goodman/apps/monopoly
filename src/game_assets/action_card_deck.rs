use rand::seq::SliceRandom;
use rand::SeedableRng;
use rand_pcg::Pcg32;
use std::str::FromStr;

use super::action_parser::Action;

#[derive(Clone, Debug, PartialEq)]
pub struct ActionCard {
    pub text: String,
    pub player_action: Action,
}

impl ActionCard {
    pub fn new(text: String, player_action: &str) -> Self {
        let player_action = Action::from_str(player_action).unwrap();
        ActionCard {
            text,
            player_action,
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct ActionCardDeck {
    init: Vec<ActionCard>,
    cards: Vec<ActionCard>,
    rng: rand_pcg::Lcg64Xsh32,
}

impl ActionCardDeck {
    pub fn new(cards: Vec<ActionCard>, seed: u64) -> Self {
        let rng: rand_pcg::Lcg64Xsh32 = Pcg32::seed_from_u64(seed);

        let mut deck = ActionCardDeck {
            init: cards.clone(),
            cards,
            rng,
        };

        deck.shuffle_deck();

        deck
    }

    pub fn shuffle_deck(&mut self) {
        self.cards = self.init.clone();
        self.cards.shuffle(&mut self.rng);
    }

    pub fn draw_card(&mut self) -> ActionCard {
        if self.cards.is_empty() {
            self.shuffle_deck();
        }

        self.cards.pop().unwrap()
    }
}

#[cfg(test)]

mod tests {
    use crate::game_assets::{config_parser::GameConfig, GameAssets};

    #[test]
    fn test_chance_card_deck() {
        let config_yaml = std::fs::read_to_string(std::path::PathBuf::from("config.yaml"))
            .expect("Failed to read config.yaml");
        let parsed_config = GameConfig::from_yaml(&config_yaml);
        let game_assets = GameAssets::new(parsed_config, Some(0));

        let mut deck = game_assets.chance_deck.clone();
        let init_len = deck.cards.len();

        // draw a card at random from the deck
        let drawn_card = deck.draw_card();

        assert_eq!(deck.cards.len(), init_len - 1);
        assert_eq!(drawn_card.text, "Go Back Three Spaces");

        // shuffle the deck
        deck.shuffle_deck();

        assert_eq!(deck.cards.len(), init_len);
    }
}
