use serde::{Deserialize, Serialize};
use std::fmt::Debug;

use tracing::debug;

use super::{
    action_parser::Action,
    config_parser::{JailCellConfig, PropertyCellConfig, RailroadCellConfig, UtilityCellConfig},
    PlayerToken,
};

pub trait GameCell: Debug {
    fn name(&self) -> String;
    fn loc(&self) -> i32;
    fn cell_type(&self) -> CellType;
    fn as_purchaseable(&self) -> Option<&dyn Purchaseable>;
    fn as_purchaseable_mut(&mut self) -> Option<&mut dyn Purchaseable>;
    fn as_upgradeable(&self) -> Option<&dyn Improvable>;
    fn as_upgradeable_mut(&mut self) -> Option<&mut dyn Improvable>;
    fn as_actionable(&self) -> Option<&dyn Actionable>;
    fn as_actionable_mut(&mut self) -> Option<&mut dyn Actionable>;
}

pub trait Purchaseable: GameCell {
    fn cost(&self) -> i32;
    fn mortgage_value(&self) -> i32;
    fn owner(&self) -> Option<PlayerToken>;
    fn set_owner(&mut self, owner: Option<PlayerToken>);
    fn mortgage(&mut self);
    fn unmortgage(&mut self);
    fn get_mortgage(&self) -> bool;
    fn get_rent(&self) -> Vec<i32>;
    fn get_owner(&self) -> Option<PlayerToken>;
}

pub trait Improvable: Purchaseable {
    fn get_improvement_tier(&self) -> i32;
    fn set_improvement_tier(&mut self, tier: i32);
    fn next_improvement_cost(&self) -> i32;
    fn total_improvement_cost(&self) -> i32;
    fn get_color_group(&self) -> String;
}

pub trait Actionable: GameCell {
    fn player_action(&self) -> Action;
}

#[derive(Clone, Debug, PartialEq)]
pub struct ActionCell {
    pub name: String,
    pub loc: i32,
    pub player_action: Action,
}

impl GameCell for ActionCell {
    fn name(&self) -> String {
        self.name.clone()
    }

    fn loc(&self) -> i32 {
        self.loc
    }

    fn cell_type(&self) -> CellType {
        CellType::Action
    }

    fn as_purchaseable(&self) -> Option<&dyn Purchaseable> {
        None
    }

    fn as_upgradeable(&self) -> Option<&dyn Improvable> {
        None
    }

    fn as_purchaseable_mut(&mut self) -> Option<&mut dyn Purchaseable> {
        None
    }

    fn as_upgradeable_mut(&mut self) -> Option<&mut dyn Improvable> {
        None
    }

    fn as_actionable(&self) -> Option<&dyn Actionable> {
        Some(self)
    }

    fn as_actionable_mut(&mut self) -> Option<&mut dyn Actionable> {
        Some(self)
    }
}

impl Actionable for ActionCell {
    fn player_action(&self) -> Action {
        self.player_action.clone()
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct PropertyCell {
    pub name: String,
    pub loc: i32,
    pub group: String,
    pub cost: i32,
    pub rent: Vec<i32>,
    pub house_cost: i32,
    pub hotel_cost: i32,
    pub mortgage_value: i32,
    pub owner: Option<PlayerToken>,
    pub improvement_tier: i32,
    pub is_mortgaged: bool,
}

impl PropertyCell {
    pub fn from_config(config: PropertyCellConfig) -> Self {
        PropertyCell {
            name: config.name,
            loc: config.loc,
            group: config.group,
            cost: config.cost,
            rent: config.rent,
            house_cost: config.house_cost,
            hotel_cost: config.hotel_cost,
            mortgage_value: config.mortgage_value,
            owner: None,
            // 0 is unimproved, 1-4 are houses, 5 is a hotel
            improvement_tier: 0,
            is_mortgaged: false,
        }
    }
}

impl Improvable for PropertyCell {
    fn get_improvement_tier(&self) -> i32 {
        self.improvement_tier
    }

    fn set_improvement_tier(&mut self, tier: i32) {
        if tier < 0 || tier > 5 {
            panic!("Invalid improvement tier: {}", tier);
        }
        self.improvement_tier = tier;
    }

    fn get_color_group(&self) -> String {
        self.group.clone()
    }

    fn next_improvement_cost(&self) -> i32 {
        match self.improvement_tier {
            // 0 is unimproved
            0 => self.house_cost,
            1 => self.house_cost,
            2 => self.house_cost,
            3 => self.house_cost,
            4 => self.hotel_cost,
            _ => self.hotel_cost,
        }
    }

    fn total_improvement_cost(&self) -> i32 {
        match self.improvement_tier {
            // 0 is unimproved
            0 => 0,
            1 => self.house_cost,
            2 => self.house_cost * 2,
            3 => self.house_cost * 3,
            4 => self.house_cost * 4,
            _ => self.hotel_cost + self.house_cost * 4,
        }
    }
}

impl Purchaseable for PropertyCell {
    fn cost(&self) -> i32 {
        self.cost
    }

    fn mortgage_value(&self) -> i32 {
        self.mortgage_value
    }

    fn owner(&self) -> Option<PlayerToken> {
        self.owner.clone()
    }

    fn set_owner(&mut self, owner: Option<PlayerToken>) {
        debug!("Setting owner of {} to {:?}", self.name, owner);
        self.owner = owner;
    }

    fn mortgage(&mut self) {
        self.is_mortgaged = true;
    }

    fn unmortgage(&mut self) {
        self.is_mortgaged = false;
    }

    fn get_mortgage(&self) -> bool {
        self.is_mortgaged
    }

    fn get_rent(&self) -> Vec<i32> {
        self.rent.clone()
    }

    fn get_owner(&self) -> Option<PlayerToken> {
        self.owner.clone()
    }
}

impl GameCell for PropertyCell {
    fn name(&self) -> String {
        self.name.clone()
    }

    fn loc(&self) -> i32 {
        self.loc
    }

    fn cell_type(&self) -> CellType {
        CellType::Property
    }

    fn as_purchaseable(&self) -> Option<&dyn Purchaseable> {
        Some(self)
    }

    fn as_upgradeable(&self) -> Option<&dyn Improvable> {
        Some(self)
    }

    fn as_purchaseable_mut(&mut self) -> Option<&mut dyn Purchaseable> {
        Some(self)
    }

    fn as_upgradeable_mut(&mut self) -> Option<&mut dyn Improvable> {
        Some(self)
    }

    fn as_actionable(&self) -> Option<&dyn Actionable> {
        None
    }

    fn as_actionable_mut(&mut self) -> Option<&mut dyn Actionable> {
        None
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct RailroadCell {
    pub name: String,
    pub loc: i32,
    pub cost: i32,
    pub rent: Vec<i32>,
    pub mortgage_value: i32,
    pub owner: Option<PlayerToken>,
    pub is_mortgaged: bool,
}

impl Purchaseable for RailroadCell {
    fn cost(&self) -> i32 {
        self.cost
    }

    fn mortgage_value(&self) -> i32 {
        self.mortgage_value
    }

    fn owner(&self) -> Option<PlayerToken> {
        self.owner.clone()
    }

    fn set_owner(&mut self, owner: Option<PlayerToken>) {
        self.owner = owner;
    }

    fn mortgage(&mut self) {
        self.is_mortgaged = true;
    }

    fn unmortgage(&mut self) {
        self.is_mortgaged = false;
    }

    fn get_mortgage(&self) -> bool {
        self.is_mortgaged
    }

    fn get_rent(&self) -> Vec<i32> {
        self.rent.clone()
    }

    fn get_owner(&self) -> Option<PlayerToken> {
        self.owner.clone()
    }
}

impl GameCell for RailroadCell {
    fn name(&self) -> String {
        self.name.clone()
    }

    fn loc(&self) -> i32 {
        self.loc
    }

    fn cell_type(&self) -> CellType {
        CellType::Railroad
    }

    fn as_purchaseable(&self) -> Option<&dyn Purchaseable> {
        Some(self)
    }

    fn as_upgradeable(&self) -> Option<&dyn Improvable> {
        None
    }

    fn as_purchaseable_mut(&mut self) -> Option<&mut dyn Purchaseable> {
        Some(self)
    }

    fn as_upgradeable_mut(&mut self) -> Option<&mut dyn Improvable> {
        None
    }

    fn as_actionable(&self) -> Option<&dyn Actionable> {
        None
    }

    fn as_actionable_mut(&mut self) -> Option<&mut dyn Actionable> {
        None
    }
}

impl RailroadCell {
    pub fn from_config(config: RailroadCellConfig) -> Self {
        RailroadCell {
            name: config.name,
            loc: config.loc,
            cost: config.cost,
            rent: config.rent,
            mortgage_value: config.mortgage_value,
            owner: None,
            is_mortgaged: false,
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct UtilityCell {
    pub name: String,
    pub loc: i32,
    pub cost: i32,
    pub rent: Vec<i32>,
    pub mortgage_value: i32,
    pub is_mortgaged: bool,
    pub owner: Option<PlayerToken>,
}

impl Purchaseable for UtilityCell {
    fn cost(&self) -> i32 {
        self.cost
    }

    fn mortgage_value(&self) -> i32 {
        self.mortgage_value
    }

    fn owner(&self) -> Option<PlayerToken> {
        self.owner.clone()
    }

    fn set_owner(&mut self, owner: Option<PlayerToken>) {
        self.owner = owner;
    }

    fn mortgage(&mut self) {
        self.is_mortgaged = true;
    }

    fn unmortgage(&mut self) {
        self.is_mortgaged = false;
    }

    fn get_mortgage(&self) -> bool {
        self.is_mortgaged
    }

    fn get_rent(&self) -> Vec<i32> {
        self.rent.clone()
    }

    fn get_owner(&self) -> Option<PlayerToken> {
        self.owner.clone()
    }
}

impl GameCell for UtilityCell {
    fn name(&self) -> String {
        self.name.clone()
    }

    fn loc(&self) -> i32 {
        self.loc
    }

    fn cell_type(&self) -> CellType {
        CellType::Utility
    }

    fn as_purchaseable(&self) -> Option<&dyn Purchaseable> {
        Some(self)
    }

    fn as_upgradeable(&self) -> Option<&dyn Improvable> {
        None
    }

    fn as_purchaseable_mut(&mut self) -> Option<&mut dyn Purchaseable> {
        Some(self)
    }

    fn as_upgradeable_mut(&mut self) -> Option<&mut dyn Improvable> {
        None
    }

    fn as_actionable(&self) -> Option<&dyn Actionable> {
        None
    }

    fn as_actionable_mut(&mut self) -> Option<&mut dyn Actionable> {
        None
    }
}

impl UtilityCell {
    pub fn from_config(config: UtilityCellConfig) -> Self {
        UtilityCell {
            name: config.name,
            loc: config.loc,
            cost: config.cost,
            rent: config.rent,
            mortgage_value: config.mortgage_value,
            is_mortgaged: false,
            owner: None,
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct JailCell {
    pub name: String,
    pub loc: i32,
}

impl GameCell for JailCell {
    fn name(&self) -> String {
        self.name.clone()
    }

    fn loc(&self) -> i32 {
        self.loc
    }

    fn cell_type(&self) -> CellType {
        CellType::Jail
    }

    fn as_purchaseable(&self) -> Option<&dyn Purchaseable> {
        None
    }

    fn as_upgradeable(&self) -> Option<&dyn Improvable> {
        None
    }

    fn as_purchaseable_mut(&mut self) -> Option<&mut dyn Purchaseable> {
        None
    }

    fn as_upgradeable_mut(&mut self) -> Option<&mut dyn Improvable> {
        None
    }

    fn as_actionable(&self) -> Option<&dyn Actionable> {
        None
    }

    fn as_actionable_mut(&mut self) -> Option<&mut dyn Actionable> {
        None
    }
}

impl JailCell {
    pub fn from_config(config: JailCellConfig) -> Self {
        JailCell {
            name: config.name,
            loc: config.loc,
        }
    }
}

#[derive(Clone, Copy, Debug, Deserialize, Serialize, PartialEq)]
pub enum CellType {
    Property,
    Railroad,
    Utility,
    Action,
    Jail,
}

#[derive(Clone, Debug, PartialEq)]
pub enum Cell {
    Property(PropertyCell),
    Railroad(RailroadCell),
    Utility(UtilityCell),
    Action(ActionCell),
    Jail(JailCell),
}

impl GameCell for Cell {
    fn name(&self) -> String {
        match self {
            Cell::Property(cell) => cell.name.clone(),
            Cell::Railroad(cell) => cell.name.clone(),
            Cell::Utility(cell) => cell.name.clone(),
            Cell::Action(cell) => cell.name.clone(),
            Cell::Jail(cell) => cell.name.clone(),
        }
    }

    fn loc(&self) -> i32 {
        match self {
            Cell::Property(cell) => cell.loc,
            Cell::Railroad(cell) => cell.loc,
            Cell::Utility(cell) => cell.loc,
            Cell::Action(cell) => cell.loc,
            Cell::Jail(cell) => cell.loc,
        }
    }

    fn cell_type(&self) -> CellType {
        match self {
            Cell::Property(_) => CellType::Property,
            Cell::Railroad(_) => CellType::Railroad,
            Cell::Utility(_) => CellType::Utility,
            Cell::Action(_) => CellType::Action,
            Cell::Jail(_) => CellType::Jail,
        }
    }

    fn as_purchaseable(&self) -> Option<&dyn Purchaseable> {
        match self {
            Cell::Property(cell) => Some(cell),
            Cell::Railroad(cell) => Some(cell),
            Cell::Utility(cell) => Some(cell),
            Cell::Action(_) => None,
            Cell::Jail(_) => None,
        }
    }

    fn as_upgradeable(&self) -> Option<&dyn Improvable> {
        match self {
            Cell::Property(cell) => Some(cell),
            Cell::Railroad(_) => None,
            Cell::Utility(_) => None,
            Cell::Action(_) => None,
            Cell::Jail(_) => None,
        }
    }

    fn as_purchaseable_mut(&mut self) -> Option<&mut dyn Purchaseable> {
        match self {
            Cell::Property(cell) => Some(cell),
            Cell::Railroad(cell) => Some(cell),
            Cell::Utility(cell) => Some(cell),
            Cell::Action(_) => None,
            Cell::Jail(_) => None,
        }
    }

    fn as_upgradeable_mut(&mut self) -> Option<&mut dyn Improvable> {
        match self {
            Cell::Property(cell) => Some(cell),
            Cell::Railroad(_) => None,
            Cell::Utility(_) => None,
            Cell::Action(_) => None,
            Cell::Jail(_) => None,
        }
    }

    fn as_actionable(&self) -> Option<&dyn Actionable> {
        match self {
            Cell::Property(_) => None,
            Cell::Railroad(_) => None,
            Cell::Utility(_) => None,
            Cell::Action(cell) => Some(cell),
            Cell::Jail(_) => None,
        }
    }

    fn as_actionable_mut(&mut self) -> Option<&mut dyn Actionable> {
        match self {
            Cell::Property(_) => None,
            Cell::Railroad(_) => None,
            Cell::Utility(_) => None,
            Cell::Action(cell) => Some(cell),
            Cell::Jail(_) => None,
        }
    }
}
