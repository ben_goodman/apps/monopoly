use std::str::FromStr;

use action_card_deck::{ActionCard, ActionCardDeck};
use cell::{ActionCell, Cell, JailCell, PropertyCell, RailroadCell, UtilityCell};
use cell_collection::CellCollection;
use config_parser::{GameConfig, GameRulesConfig};
use dice::{Dice, Die};
use serde::{Deserialize, Serialize};

pub(crate) mod action_card_deck;
pub(crate) mod action_parser;
pub(crate) mod cell;
pub(crate) mod cell_collection;
pub(crate) mod config_parser;
pub(crate) mod dice;

#[derive(Debug, Clone, Copy, Hash, Eq, PartialEq, Serialize, Deserialize)]
pub enum PlayerToken {
    Battleship,
    Boot,
    Cat,
    Dog,
    Hat,
    Iron,
    RaceCar,
    Thimble,
    Wheelbarrow,
}

#[derive(Clone, Debug, PartialEq)]
pub struct GameRules {
    pub starting_balance: i32,
    pub pass_go_bonus: i32,
    pub jail_fine: i32,
    pub jail_turns: i32,
    pub speed_limit: i32,
    pub bank_free_parking: bool,
    pub enable_auctions: bool,
    pub monopoly_rent_multiplier: i32,
    pub house_count: i32,
    pub hotel_count: i32,
}

impl GameRules {
    pub fn from_config(config: GameRulesConfig) -> Self {
        GameRules {
            starting_balance: config.starting_balance,
            pass_go_bonus: config.pass_go_bonus,
            jail_fine: config.jail_fine,
            jail_turns: config.jail_turns,
            speed_limit: config.speed_limit,
            bank_free_parking: config.bank_free_parking,
            enable_auctions: config.enable_auctions,
            monopoly_rent_multiplier: config.monopoly_rent_multiplier,
            house_count: config.house_count,
            hotel_count: config.hotel_count,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct GameAssets {
    // pub random_seed: u64,
    pub rules: GameRules,
    pub dice: Dice,
    pub chance_deck: ActionCardDeck,
    pub community_chest_deck: ActionCardDeck,
    pub cells: CellCollection<Cell>,
}

impl GameAssets {
    pub fn new(config_yaml: GameConfig, seed: Option<u64>) -> Self {
        let random_seed = seed.unwrap_or(config_yaml.simulation.seed.unwrap_or(0));

        // let random_seed = seed.unwrap_or(config_yaml.simulation.seed);
        // collect all the cells into a single vector
        let mut cells = Vec::new();
        for cell in config_yaml.property_cells {
            let property_cell = PropertyCell::from_config(cell);
            cells.push(Cell::Property(property_cell));
        }
        for cell in config_yaml.railroad_cells {
            // cells.push(Cell::Railroad(cell))
            let rr_cell = RailroadCell::from_config(cell);
            cells.push(Cell::Railroad(rr_cell));
        }
        for cell in config_yaml.utility_cells {
            let utility_cell = UtilityCell::from_config(cell);
            cells.push(Cell::Utility(utility_cell));
        }
        for cell in config_yaml.action_cells {
            let player_action = action_parser::Action::from_str(&cell.player_action).unwrap();
            cells.push(Cell::Action(ActionCell {
                name: cell.name,
                loc: cell.loc,
                player_action,
            }));
        }
        for cell in config_yaml.jail_cells {
            cells.push(Cell::Jail(JailCell::from_config(cell)));
        }

        // parse card actions in the chance and community chest decks
        let mut chance_cards = Vec::new();
        for card in config_yaml.chance_deck {
            chance_cards.push(ActionCard::new(card.text, &card.player_action));
        }

        let mut community_chest_cards = Vec::new();
        for card in config_yaml.community_chest_deck {
            community_chest_cards.push(ActionCard::new(card.text, &card.player_action));
        }

        GameAssets {
            // random_seed,
            rules: GameRules::from_config(config_yaml.rules),
            chance_deck: ActionCardDeck::new(chance_cards, random_seed),
            community_chest_deck: ActionCardDeck::new(community_chest_cards, random_seed),
            cells: CellCollection::new(cells),
            dice: Dice::new(
                config_yaml
                    .dice
                    .iter()
                    .map(|die| Die {
                        min: die.min,
                        max: die.max,
                    })
                    .collect(),
                random_seed,
            ),
        }
    }
}
