use std::collections::HashMap;

use game_assets::{config_parser::GameConfig, PlayerToken};
use game_error::GameErrorKind;
// use game_assets::PlayerToken;
use player::PlayerProfile;
use statistics::GameStatistics;
use tracing::info;

mod advisor;
mod bank;
mod game_assets;
mod game_error;
mod player;
mod statistics;
mod trader;

use crate::game_assets::GameAssets;

pub fn simulate_game(config_yaml: String, seed: Option<u64>) -> GameStatistics {
    let game_config = GameConfig::from_yaml(&config_yaml);

    // the game seed is used to generate random numbers for dice rolls and card decks.
    // if no seed is provided, one is generated at random
    // in order:
    // 1. from the command line
    // 2. from the config file
    // 3. at random
    let game_seed = seed.unwrap_or_else(|| game_config.simulation.seed.unwrap_or_else(rand::random));


    let simulation_config = game_config.simulation.clone();
    let players_config = game_config.players.clone();
    let game_assets = GameAssets::new(game_config, Some(game_seed));

    // create a vector of players to loop through
    let mut players = players_config
        .iter()
        .map(|player_config| {
            // players each have their own rng seed
            player::Player::new(player_config.token, &simulation_config, player_config.seed)
        })
        .collect::<Vec<player::Player>>();

    // get player profiles
    let player_profiles: HashMap<PlayerToken, PlayerProfile> = players
        .iter()
        .map(|player| (player.token, player.get_profile()))
        .collect();

    let mut bank = bank::Bank::new(players.clone(), game_assets);

    let mut turn_counter = 0;
    let mut statistics = statistics::GameStatistics::new(player_profiles, game_seed);

    loop {
        // break if the game has gone on for too long
        if turn_counter >= simulation_config.max_iterations {
            info!(
                "Game over after {} turns. Max iterations reached.",
                turn_counter
            );
            break;
        }
        // execute the turn for each player
        // repeat until the game is over
        for player in players.iter_mut() {
            let mut turn_metrics = statistics::TurnMetrics::new(turn_counter);
            turn_counter += 1;

            // if the player is bankrupt, remove them from the game
            match player.execute_turn(&mut bank, &mut turn_metrics) {
                Ok(updated_metrics) => {
                    statistics.record_turn(updated_metrics);
                }
                Err((e, updated_metrics)) => {
                    if e.kind() == GameErrorKind::InsufficientFunds {
                        info!("Player {:?} is bankrupt", player.token);
                        player.is_bankrupt = true;
                        statistics.record_turn(updated_metrics);
                        bank.remove_player(player.token).expect("Failed to remove player");

                        // check if the game is over and break the loop if needed
                        if bank.player_count() == 1 {
                            break;
                        }
                    } else {
                        panic!("Unexpected error: {:?}", e);
                    }
                }
            }
        }

        // check if the game is over
        // the game ends when there is only one player left
        // otherwise, continue to the next turn
        players.retain(|player| !player.is_bankrupt);

        if players.len() == 1 {
            info!(
                "Game over after {} turns. Winner: {:?}",
                turn_counter, players[0].token
            );
            break;
        }
    }

    statistics

}