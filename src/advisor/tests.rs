#[cfg(test)]

mod tests {
    // use tracing::Level;
    // use tracing_subscriber::FmtSubscriber;

    use crate::{
        advisor::Advisor,
        bank::Bank,
        game_assets::{config_parser::GameConfig, GameAssets},
        player::Player,
    };

    macro_rules! setup_test {
        () => {{
            // let subscriber = FmtSubscriber::builder()
            //     .with_max_level(Level::TRACE)
            //     .finish();

            // tracing::subscriber::set_global_default(subscriber)
            //     .expect("setting default subscriber failed");

            let config_yaml = std::fs::read_to_string(std::path::PathBuf::from("config.yaml"))
                .expect("Failed to read config.yaml");

            let game_config = GameConfig::from_yaml(&config_yaml);
            let simulation_config = game_config.simulation.clone();
            let players_config = game_config.players.clone();
            let game_assets = GameAssets::new(game_config, Some(0));

            // create a vector of players to loop through
            let players = players_config
                .iter()
                .map(|player_config| {
                    Player::new(player_config.token, &simulation_config, player_config.seed)
                })
                .collect::<Vec<Player>>();

            // let players_clone = players.clone();
            let game_assets = game_assets;
            let bank = Bank::new(players.clone(), game_assets);

            (players.clone(), bank)
        }};
    }

    #[test]
    fn test_estimate_property_worth() {
        let (players, mut bank) = setup_test!();

        let player= &players[0];

        let worth_property_1 = Advisor::estimate_property_worth(
            1,
            player,
            player.get_profile().evaluation_accuracy,
            &bank,
        )
        .unwrap();
        let worth_property_2 = Advisor::estimate_property_worth(
            3,
            player,
            player.get_profile().evaluation_accuracy,
            &bank,
        )
        .unwrap();

        assert_eq!(worth_property_1, 85);
        assert_eq!(worth_property_2, 85);

        // purchase the property
        bank.purchase_property(player, 1, None)
            .expect("Failed to purchase property");

        // re-estimate the property worth
        let worth_property_2 = Advisor::estimate_property_worth(
            3,
            player,
            player.get_profile().evaluation_accuracy,
            &bank,
        )
        .unwrap();

        assert_eq!(worth_property_2, 263);

        // purchase the property
        bank.purchase_property(player, 3, None)
            .expect("Failed to purchase property");

        // re-estimate the property worth
        let worth_property_2 = Advisor::estimate_property_worth(
            3,
            player,
            player.get_profile().evaluation_accuracy,
            &bank,
        )
        .unwrap();
        println!("worth_property_2: {}", worth_property_2);
    }

    #[test]
    fn test_recommend_player_trades() {
        let (players, mut bank) = setup_test!();

        let player = &players[0];

        let player_accuracy = player.get_profile().evaluation_accuracy;
        let player_trade_multiplier = player.get_profile().property_trade_multiplier;
        let recommended_trades = Advisor::recommend_player_trades(
            player,
            &bank,
            player_accuracy,
            player_trade_multiplier,
        )
        .expect("Failed to recommend player trades");

        assert_eq!(recommended_trades.len(), 0);

        // player 2 purchases property 1
        bank.purchase_property(&players[1], 1, None)
            .expect("Failed to purchase property");

        let recommended_trades = Advisor::recommend_player_trades(
            player,
            &bank,
            player_accuracy,
            player_trade_multiplier,
        )
        .expect("Failed to recommend player trades");

        println!("recommended_trades: {:?}", recommended_trades);

        // player 1 purchases property 3
        bank.purchase_property(&players[0], 3, None)
            .expect("Failed to purchase property");

        let recommended_trades = Advisor::recommend_player_trades(
            player,
            &bank,
            player_accuracy,
            player_trade_multiplier,
        )
        .expect("Failed to recommend player trades");

        println!("recommended_trades: {:?}", recommended_trades);
    }
}
