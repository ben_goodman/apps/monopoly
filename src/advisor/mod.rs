use std::ops::{Add, Mul, Sub};

use crate::{
    bank::Bank,
    game_assets::{
        cell::{CellType, GameCell},
        PlayerToken,
    },
    game_error::GameError,
    player::Player,
};

mod tests;

pub struct Advisor {}

impl Advisor {
    /// Estimates the worth of a property based on the player's evaluation accuracy.
    /// Players will use their property evaluations to make decisions on trades.
    pub fn estimate_property_worth(
        location: i32,
        player: &Player,
        accuracy: f32,
        bank: &Bank,
    ) -> Result<i32, GameError> {
        let property_cell = bank
            .game_assets
            .cells
            .get_cell_at(location)
            .ok_or(GameError::CellNotFound(location))?;

        let cell_type = property_cell.cell_type().clone();
        let purchaseable_cell = property_cell
            .as_purchaseable()
            .ok_or(GameError::PurchaseableRequired(cell_type))?;

        let cost = purchaseable_cell.cost();

        let actual_value = match cell_type {
            // for improvable properties
            // does the player already have a property in the same color group?
            CellType::Property => {
                let property_cell = purchaseable_cell.as_upgradeable().unwrap();
                let color_group = property_cell.get_color_group();

                let color_group_properties = bank
                    .game_assets
                    .cells
                    .get_cells_by_color_group(&color_group);
                let color_group_properties_len = color_group_properties.len();

                let mut player_owned_group_count: i32 = 0;

                for property in color_group_properties {
                    if property.owner().as_ref() == Some(&player.token) {
                        player_owned_group_count += 1;
                    }
                }

                // does the property complete a monopoly or does the player already own the property?
                let monopoly_value_multiplier = match property_cell.owner() == Some(player.token)
                    || color_group_properties_len - player_owned_group_count as usize == 1
                {
                    true => 2.0,
                    false => 1.0,
                };

                // the perceived value of the property is proportional to the number
                // of properties currently owned in the color group
                let value = (cost as f32)
                    * (1.0 + player_owned_group_count as f32)
                    * monopoly_value_multiplier;

                Ok(value)
            }

            // for railroads
            // how many railroads does the player own?
            CellType::Railroad => {
                let player_owned_railroads_count = bank
                    .game_assets
                    .cells
                    .get_cells_by_owner(&player.token)
                    .iter()
                    .filter(|cell| cell.cell_type() == CellType::Railroad)
                    .count();

                // the perceived value of the railroad is proportional to the number
                // of railroads currently owned
                let value = cost.mul(1 + player_owned_railroads_count as i32);

                Ok(value as f32)
            }

            // for utilities
            // how many utilities does the player own?
            CellType::Utility => {
                let player_owned_utilities_count = bank
                    .game_assets
                    .cells
                    .get_cells_by_owner(&player.token)
                    .iter()
                    .filter(|cell| cell.cell_type() == CellType::Railroad)
                    .count();

                let value = cost.mul(1 + player_owned_utilities_count as i32);

                Ok(value as f32)
            }
            _ => Err(GameError::InvalidCellType(cell_type.clone())),
        }?;

        let perceived_value = match accuracy > 0.0 {
            true => actual_value.add(actual_value as f32 * accuracy),
            false => actual_value.sub(actual_value as f32 * accuracy.abs()).abs(),
        }
        .floor() as i32;

        Ok(perceived_value)
    }

    /// Advise the player on which trades to make.
    /// A trade is a purchase of a property from another player.
    /// Players prioritize trades based on the perceived value of the property
    pub fn recommend_player_trades(
        player: &Player,
        bank: &Bank,
        accuracy: f32,
        property_trade_multiplier: f32,
    ) -> Result<Vec<(i32, f32)>, GameError> {
        // a trade is a tuple of (property, value)
        let mut trades: Vec<(i32, f32)> = Vec::new();

        // get all owned properties, by any other player
        // that have no improvements.  Mortgaged properties are eligible to trade.
        let mut eligible_locations: Vec<i32> = vec![];

        // unimproved properties owned by other players.
        let mut eligible_street_locations = bank
            .game_assets
            .cells
            .get_cells_by_type(&CellType::Property)
            .iter()
            .filter(|cell| {
                let binding = cell
                    .as_upgradeable()
                    .expect("Expected upgradeable cell.")
                    .owner();
                let owner = binding.as_ref();
                owner.is_some() && owner != Some(&player.token)
            })
            .map(|cell| cell.as_upgradeable().unwrap())
            .filter(|cell| cell.get_improvement_tier() == 0)
            .map(|cell| cell.loc())
            .collect::<Vec<i32>>();

        eligible_locations.append(&mut eligible_street_locations);

        // railroads owned by other players
        let mut eligible_railroad_locations = bank
            .game_assets
            .cells
            .get_cells_by_type(&CellType::Railroad)
            .iter()
            .filter(|cell| {
                let binding = cell
                    .as_purchaseable()
                    .expect("Expected ownable cell.")
                    .owner();
                let owner = binding.as_ref();
                owner.is_some() && owner != Some(&player.token)
            })
            .map(|cell| cell.loc())
            .collect::<Vec<i32>>();

        eligible_locations.append(&mut eligible_railroad_locations);

        // utilities owned by other players
        let mut eligible_utility_locations = bank
            .game_assets
            .cells
            .get_cells_by_type(&CellType::Utility)
            .iter()
            .filter(|cell| {
                let binding = cell
                    .as_purchaseable()
                    .expect("Expected ownable cell.")
                    .owner();
                let owner = binding.as_ref();
                owner.is_some() && owner != Some(&player.token)
            })
            .map(|cell| cell.loc())
            .collect::<Vec<i32>>();

        eligible_locations.append(&mut eligible_utility_locations);

        // appraise each property by their perceived value.
        for location in eligible_locations {
            let amount =
                Advisor::estimate_property_worth(location, &player, accuracy, bank)? as f32;
            let adjusted_amount = amount.mul(property_trade_multiplier) as i32;
            // let reserve_percentage = bank.player_profiles[&player].cash_reserve_percentage;
            // if the player can afford the potential trade, add it to the list of trades
            match bank.assert_sufficient_player_funds(
                &player,
                adjusted_amount,
                Some(player.cash_reserve_percentage),
            ) {
                Ok(_) => trades.push((location, amount as f32)),
                Err(_) => continue,
            }
        }

        // sort trades by value
        trades.sort_by(|a, b| a.1.partial_cmp(&b.1).unwrap());

        Ok(trades)
    }
}
