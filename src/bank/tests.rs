#[cfg(test)]

mod tests {
    use crate::bank::Bank;
    use crate::game_assets::cell::GameCell;
    use crate::game_assets::config_parser::GameConfig;
    use crate::game_assets::dice::Dice;
    use crate::game_assets::dice::Die;
    use crate::game_assets::GameAssets;
    use crate::game_assets::PlayerToken;
    use crate::player::{Player, PlayerProfile};
    use std::collections::HashMap;

    // use tracing::Level;
    // use tracing_subscriber::FmtSubscriber;

    // a macro for setting up each test
    macro_rules! setup_test {
        () => {{
            // let subscriber = FmtSubscriber::builder()
            //     .with_max_level(Level::TRACE)
            //     .finish();

            // tracing::subscriber::set_global_default(subscriber)
            //     .expect("setting default subscriber failed");

            let config_yaml = std::fs::read_to_string(std::path::PathBuf::from("config.yaml"))
                .expect("Failed to read config.yaml");

            let game_config = GameConfig::from_yaml(&config_yaml);
            let simulation_config = game_config.simulation.clone();
            let players_config = game_config.players.clone();
            let game_assets = GameAssets::new(game_config, Some(0));

            // create a vector of players to loop through
            let players = players_config
                .iter()
                .map(|player_config| {
                    Player::new(player_config.token, &simulation_config, player_config.seed)
                })
                .collect::<Vec<Player>>();

            // let players_clone = players.clone();
            let game_assets = game_assets;
            let bank = Bank::new(players.clone(), game_assets);

            (players.clone(), bank)
        }};
    }

    #[test]
    fn test_bank_new() {
        let (players, bank) = setup_test!();

        assert_eq!(bank.balances.len(), 5);
        assert_eq!(bank.balances.get(&players[0].token).unwrap(), &1500);
        assert_eq!(bank.balances.get(&players[1].token).unwrap(), &1500);
    }

    #[test]
    fn test_player_transfer() {
        let (players, mut bank) = setup_test!();

        bank.player_to_player_transfer(players[0].token, players[1].token, 500)
            .unwrap();
        assert_eq!(bank.balances.get(&players[0].token).unwrap(), &1000);
        assert_eq!(bank.balances.get(&players[1].token).unwrap(), &2000);
    }

    #[test]
    fn test_player_transfer_raise_funds() {
        let (players, mut bank) = setup_test!();

        bank.purchase_property(&players[0], 1, None)
            .expect("Failed to purchase property");
        bank.purchase_property(&players[0], 3, None)
            .expect("Failed to purchase property");

        bank.improve_property_group(players[0].token, "brown")
            .unwrap();
        bank.improve_property_group(players[0].token, "brown")
            .unwrap();

        // set player balance to 0 for testing
        bank.balances.insert(players[0].token, 0);

        let p2_initial_balance = bank
            .get_player_balance(players[1].token)
            .expect("failed to get player bal.");

        // to raise $60, the player will need to sell 2 houses each on 1,3.
        // this generates $100 income - minus 60 to player 2 leaves $40 left.
        bank.player_to_player_transfer(players[0].token, players[1].token, 60)
            .expect("Failed to transfer funds");

        assert_eq!(bank.balances.get(&players[0].token).unwrap(), &40);
        assert_eq!(
            bank.balances.get(&players[1].token).unwrap(),
            &(p2_initial_balance + 60)
        );
    }

    #[test]
    fn test_player_transfer_bankruptcy() {
        let (players, mut bank) = setup_test!();

        // set player balance to 100
        bank.balances.insert(players[0].token, 0);

        // expect to fail if player 0 has insufficient funds
        let result = bank.player_to_player_transfer(players[0].token, players[1].token, 500);
        assert!(result.is_err());
        println!("{:?}", result);
    }

    #[test]
    fn test_add_to_balance() {
        let (players, mut bank) = setup_test!();

        bank.add_to_balance(players[0].token, 500).unwrap();
        assert_eq!(bank.balances.get(&players[0].token).unwrap(), &2000);

        // expect to fail if player not found
        let result = bank.add_to_balance(PlayerToken::Iron, 500);
        assert!(result.is_err());
    }

    #[test]
    fn test_subtract_from_balance() {
        let (players, mut bank) = setup_test!();

        bank.subtract_from_balance(players[0].token, 500, &"")
            .unwrap();
        assert_eq!(bank.balances.get(&players[0].token).unwrap(), &1000);
    }

    #[test]
    fn test_subtract_from_balance_raise_funds() {
        let (players, mut bank) = setup_test!();

        bank.purchase_property(&players[0], 1, None)
            .expect("Failed to purchase property");
        bank.purchase_property(&players[0], 3, None)
            .expect("Failed to purchase property");

        bank.improve_property_group(players[0].token, "brown")
            .unwrap();
        bank.improve_property_group(players[0].token, "brown")
            .unwrap();
        bank.improve_property_group(players[0].token, "brown")
            .unwrap();
        bank.improve_property_group(players[0].token, "brown")
            .unwrap();
        bank.improve_property_group(players[0].token, "brown")
            .unwrap();

        // set player balance to 0 for testing
        bank.balances.insert(players[0].token, 0);

        bank.subtract_from_balance(players[0].token, 250, &"")
            .expect("Failed to subtract from balance");

        let improvement_level = bank
            .game_assets
            .cells
            .get_cell_at(1)
            .unwrap()
            .as_upgradeable()
            .unwrap()
            .get_improvement_tier();

        assert_eq!(improvement_level, 0);
    }

    #[test]
    fn test_purchase_property() {
        let (players, mut bank) = setup_test!();

        bank.purchase_property(&players[0], 1, None).unwrap();
        let owner = bank
            .game_assets
            .cells
            .get_cell_at(1)
            .unwrap()
            .as_purchaseable()
            .unwrap()
            .owner()
            .unwrap();
        assert_eq!(owner, players[0].token);
    }

    // #[test]
    // fn test_improve_property() {
    //     let (players, mut bank) = setup_test!();

    //     assert_eq!(bank.available_houses, 32);
    //     assert_eq!(bank.available_hotels, 12);

    //     bank.purchase_property(players[0].token, 1, None).unwrap();
    //     bank.improve_property_group(players[0].token, 1).unwrap();

    //     assert_eq!(bank.available_houses, 31);
    //     assert_eq!(bank.available_hotels, 12);

    //     // expect to fail if player does not own the property
    //     let result = bank.improve_property(players[0].token, 2);
    //     assert!(result.is_err());

    //     // expect to fail if the cell is not improvable
    //     bank.purchase_property(players[0].token, 5, None).unwrap();
    //     let result = bank.improve_property(players[0].token, 5);
    //     assert!(result.is_err());
    // }

    #[test]
    fn test_mortgage_street_property() {
        let (players, mut bank) = setup_test!();

        // expect to fail if player not found
        let result = bank.mortgage_property(PlayerToken::Cat, 1);
        assert!(result.is_err());

        // expect to fail if the cell is not mortgageable
        let result = bank.mortgage_property(players[0].token, 0);
        assert!(result.is_err());

        // expect to fail if player does not own the property
        let result = bank.mortgage_property(players[0].token, 1);
        assert!(result.is_err());

        // is purchased for 60
        bank.purchase_property(&players[0], 1, None).unwrap();
        assert_eq!(
            bank.get_player_balance(players[0].token)
                .expect("failed to get player bal."),
            1440
        );
        assert_eq!(
            bank.game_assets
                .cells
                .get_cell_at(1)
                .unwrap()
                .as_purchaseable()
                .unwrap()
                .get_owner()
                .unwrap(),
            players[0].token
        );

        // is mortgaged for 30
        bank.mortgage_property(players[0].token, 1).unwrap();
        assert_eq!(
            bank.get_player_balance(players[0].token)
                .expect("failed to get player bal."),
            1470
        );
        assert_eq!(
            bank.game_assets
                .cells
                .get_cell_at(1)
                .unwrap()
                .as_purchaseable()
                .unwrap()
                .get_owner()
                .unwrap(),
            players[0].token
        );

        // expect to fail if the cell is already mortgaged
        let result = bank.mortgage_property(players[0].token, 1);
        assert!(result.is_err());

        // unmortgage the property for the mortgage value + 10%
        bank.unmortgage_property(players[0].token, 1).unwrap();
        assert_eq!(
            bank.get_player_balance(players[0].token)
                .expect("failed to get player bal."),
            1437
        );
        assert_eq!(
            bank.game_assets
                .cells
                .get_cell_at(1)
                .unwrap()
                .as_purchaseable()
                .unwrap()
                .get_owner()
                .unwrap(),
            players[0].token
        );

        bank.mortgage_property(players[0].token, 1).unwrap();
    }

    #[test]
    fn test_mortgage_railroad_property() {
        let (players, mut bank) = setup_test!();

        // expect to fail if player not found
        let result = bank.mortgage_property(PlayerToken::Cat, 5);
        assert!(result.is_err());

        // expect to fail if the cell is not mortgageable
        let result = bank.mortgage_property(players[0].token, 0);
        assert!(result.is_err());

        // expect to fail if player does not own the property
        let result = bank.mortgage_property(players[0].token, 5);
        assert!(result.is_err());

        // is purchased for 200
        bank.purchase_property(&players[0], 5, None).unwrap();
        assert_eq!(
            bank.get_player_balance(players[0].token)
                .expect("failed to get player bal."),
            1300
        );
        assert_eq!(
            bank.game_assets
                .cells
                .get_cell_at(5)
                .unwrap()
                .as_purchaseable()
                .unwrap()
                .get_owner()
                .unwrap(),
            players[0].token
        );

        // // is mortgaged for 100
        bank.mortgage_property(players[0].token, 5)
            .expect("Failed to mortgage property");
        assert_eq!(
            bank.get_player_balance(players[0].token)
                .expect("failed to get player bal."),
            1400
        );
        assert_eq!(
            bank.game_assets
                .cells
                .get_cell_at(5)
                .unwrap()
                .as_purchaseable()
                .unwrap()
                .get_owner()
                .unwrap(),
            players[0].token
        );

        // // expect to fail if the cell is already mortgaged
        let result = bank.mortgage_property(players[0].token, 5);
        assert!(result.is_err());
    }

    #[test]
    fn test_compute_rent_improvable() {
        let (players, mut bank) = setup_test!();

        // get a dice roll
        let mut dice = Dice::new(vec![Die { min: 1, max: 6 }, Die { min: 1, max: 6 }], 0);
        let dice_roll = dice.roll();

        // player 1 purchases property 1
        bank.purchase_property(&players[0], 1, None).unwrap();

        // compute rent for property 1
        let rent = bank.compute_current_rent(1, &dice_roll).unwrap();
        assert_eq!(rent, 2);

        // player 1 purchases property 3
        bank.purchase_property(&players[0], 3, None).unwrap();

        // compute rent with monopoly
        let rent = bank.compute_current_rent(1, &dice_roll).unwrap();
        assert_eq!(rent, 4);

        // improve property 1,3
        bank.improve_property_group(players[0].token, "brown")
            .unwrap();
        // bank.improve_property(players[0].token, 3).unwrap();

        // compute rent with 1 improvements each
        let rent = bank.compute_current_rent(1, &dice_roll).unwrap();
        assert_eq!(rent, 10);

        // improve up to hotel level
        bank.improve_property_group(players[0].token, "brown")
            .unwrap();
        bank.improve_property_group(players[0].token, "brown")
            .unwrap();
        bank.improve_property_group(players[0].token, "brown")
            .unwrap();
        bank.improve_property_group(players[0].token, "brown")
            .unwrap();

        // compute rent with a hotel each
        let rent = bank.compute_current_rent(1, &dice_roll).unwrap();
        assert_eq!(rent, 250);
    }

    #[test]
    fn test_compute_rent_railroad() {
        let (players, mut bank) = setup_test!();

        // get a dice roll
        let mut dice = Dice::new(vec![Die { min: 1, max: 6 }, Die { min: 1, max: 6 }], 0);
        let dice_roll = dice.roll();

        // p1 purchases 5, 15, 25, 35
        bank.purchase_property(&players[0], 5, None).unwrap();
        assert_eq!(
            bank.compute_current_rent(5, &dice_roll)
                .expect("Expected railroad tier 1 rent."),
            25
        );

        bank.purchase_property(&players[0], 15, None).unwrap();
        assert_eq!(
            bank.compute_current_rent(5, &dice_roll)
                .expect("Expected railroad tier 2 rent."),
            50
        );
        assert_eq!(
            bank.compute_current_rent(15, &dice_roll)
                .expect("Expected railroad tier 2 rent."),
            50
        );

        bank.purchase_property(&players[0], 25, None).unwrap();
        assert_eq!(
            bank.compute_current_rent(5, &dice_roll)
                .expect("Expected railroad tier 3 rent."),
            100
        );
        assert_eq!(
            bank.compute_current_rent(15, &dice_roll)
                .expect("Expected railroad tier 3 rent."),
            100
        );
        assert_eq!(
            bank.compute_current_rent(25, &dice_roll)
                .expect("Expected railroad tier 3 rent."),
            100
        );

        bank.purchase_property(&players[0], 35, None).unwrap();
        assert_eq!(
            bank.compute_current_rent(5, &dice_roll)
                .expect("Expected railroad tier 4 rent."),
            200
        );
        assert_eq!(
            bank.compute_current_rent(15, &dice_roll)
                .expect("Expected railroad tier 3 rent."),
            200
        );
        assert_eq!(
            bank.compute_current_rent(25, &dice_roll)
                .expect("Expected railroad tier 3 rent."),
            200
        );
        assert_eq!(
            bank.compute_current_rent(35, &dice_roll)
                .expect("Expected railroad tier 3 rent."),
            200
        );
    }

    #[test]
    fn test_compute_rent_utility() {
        let (players, mut bank) = setup_test!();

        // get a dice roll
        let mut dice = Dice::new(vec![Die { min: 1, max: 6 }, Die { min: 1, max: 6 }], 0);
        // [1,1] = 2
        let dice_roll = dice.roll();

        // p1 purchases 12, 28
        // compute rent for 1 utility
        bank.purchase_property(&players[0], 12, None).unwrap();
        assert_eq!(bank.compute_current_rent(12, &dice_roll).unwrap(), 8);

        // compute rent for 2 utilities
        bank.purchase_property(&players[0], 28, None).unwrap();
        assert_eq!(bank.compute_current_rent(12, &dice_roll).unwrap(), 20);
        assert_eq!(bank.compute_current_rent(28, &dice_roll).unwrap(), 20);
    }

    #[test]
    fn test_player_raise_funds_all() {
        let (players, mut bank) = setup_test!();

        // player 1 purchases properties 1,3
        bank.purchase_property(&players[0], 1, None).unwrap();
        bank.purchase_property(&players[0], 3, None).unwrap();

        // player improves properties to hotel level
        bank.improve_property_group(players[0].token, "brown")
            .unwrap();
        bank.improve_property_group(players[0].token, "brown")
            .unwrap();
        bank.improve_property_group(players[0].token, "brown")
            .unwrap();
        bank.improve_property_group(players[0].token, "brown")
            .unwrap();
        bank.improve_property_group(players[0].token, "brown")
            .unwrap();

        // set player balance to 0
        bank.balances.insert(players[0].token, 0);

        // player raises funds by selling 1 upgrade (from each group property) back to the bank
        // until the balance is above or equal to 250
        bank.subtract_from_balance(players[0].token, 250, &"")
            .expect("Failed to subtract from balance");

        // the player should now have a balance of 0
        assert_eq!(
            bank.get_player_balance(players[0].token)
                .expect("failed to get player bal."),
            0
        );

        // the properties should now have tier 0 improvements - i.e., 0 houses
        let property_1 = bank
            .game_assets
            .cells
            .get_cell_at(1)
            .unwrap()
            .as_upgradeable()
            .unwrap();
        assert_eq!(property_1.get_improvement_tier(), 0);

        let property_2 = bank
            .game_assets
            .cells
            .get_cell_at(3)
            .unwrap()
            .as_upgradeable()
            .unwrap();
        assert_eq!(property_2.get_improvement_tier(), 0);
    }

    #[test]
    fn test_player_raise_funds_partial() {
        let (players, mut bank) = setup_test!();

        // player 1 purchases properties 1,3
        bank.purchase_property(&players[0], 1, None).unwrap();
        bank.purchase_property(&players[0], 3, None).unwrap();

        // player improves properties to hotel level
        bank.improve_property_group(players[0].token, "brown")
            .unwrap();
        bank.improve_property_group(players[0].token, "brown")
            .unwrap();
        bank.improve_property_group(players[0].token, "brown")
            .unwrap();
        bank.improve_property_group(players[0].token, "brown")
            .unwrap();
        bank.improve_property_group(players[0].token, "brown")
            .unwrap();

        // set player balance to 0
        bank.balances.insert(players[0].token, 0);

        // player raises funds by selling 1 upgrade (from each group property) back to the bank
        // until the balance is above or equal to 250
        bank.subtract_from_balance(players[0].token, 50, &"")
            .expect("Failed to subtract from balance");

        // the player should now have a balance of 0
        assert_eq!(
            bank.get_player_balance(players[0].token)
                .expect("failed to get player bal."),
            0
        );

        // the properties should now have tier 0 improvements - i.e., 0 houses
        let property_1 = bank
            .game_assets
            .cells
            .get_cell_at(1)
            .unwrap()
            .as_upgradeable()
            .unwrap();
        assert_eq!(property_1.get_improvement_tier(), 4);

        let property_2 = bank
            .game_assets
            .cells
            .get_cell_at(3)
            .unwrap()
            .as_upgradeable()
            .unwrap();
        assert_eq!(property_2.get_improvement_tier(), 4);
    }

    #[test]
    fn test_compute_player_net_worth() {
        let (players, mut bank) = setup_test!();

        // player initial net worth is 1500
        assert_eq!(
            bank.compute_player_net_worth(players[0].token).unwrap(),
            1500
        );

        // player 1 purchases properties 1,3
        bank.purchase_property(&players[0], 1, None).unwrap();
        bank.purchase_property(&players[0], 3, None).unwrap();

        // worth is cash plus property mortgage values
        assert_eq!(
            bank.compute_player_net_worth(players[0].token).unwrap(),
            1440
        );

        // // player improves properties to hotel level
        bank.improve_property_group(players[0].token, "brown")
            .unwrap();

        // worth is cash plus property values plus improvement resale values (minus cost to improve)
        assert_eq!(
            bank.compute_player_net_worth(players[0].token).unwrap(),
            1440 + 50 - 100
        );

        bank.improve_property_group(players[0].token, "brown")
            .unwrap();
        bank.improve_property_group(players[0].token, "brown")
            .unwrap();
        bank.improve_property_group(players[0].token, "brown")
            .unwrap();
        bank.improve_property_group(players[0].token, "brown")
            .unwrap();

        assert_eq!(
            bank.compute_player_net_worth(players[0].token).unwrap(),
            1190
        );
    }

    #[test]
    fn test_property_auction() {
        let (players, mut bank) = setup_test!();
        // player 0 purchases property 1
        bank.purchase_property(&players[0], 1, None).unwrap();

        bank.auction_property(3).unwrap();

        // player 1 should now own property 3
        let owner = bank
            .game_assets
            .cells
            .get_cell_at(3)
            .unwrap()
            .as_purchaseable()
            .expect("Expected purchaseable cell")
            .owner()
            .expect("Expected owner to be set");

        assert_eq!(owner, players[3].token);
    }
}
