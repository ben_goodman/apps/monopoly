// Provides the source of truth for each player's balance
// Facilitates the transfer of funds between players.
// Facilitates any transactions that mutate game assets, (i.e., mortgaging, improving, etc.)
use std::collections::HashMap;

use player_services::PlayerServices;
use property_services::PropertyServices;
use tracing::info;

use crate::advisor::Advisor;
use crate::game_assets::cell::GameCell;
use crate::game_assets::dice::DiceRoll;
use crate::game_assets::{GameAssets, PlayerToken};
use crate::game_error::GameError;
use crate::player::Player;
use crate::statistics::BankMetrics;

mod player_services;
mod property_services;
mod tests;

/// The Bank struct is the source of truth for each player's balance.
#[derive(Debug, PartialEq)]
pub struct Bank {
    balances: HashMap<PlayerToken, i32>,
    pub players: HashMap<PlayerToken, Player>,
    pub game_assets: GameAssets,
    pub available_houses: i32,
    pub available_hotels: i32,
}

impl Bank {
    pub fn new(players: Vec<Player>, game_assets: GameAssets) -> Self {
        let game_rules = game_assets.rules.clone();
        let starting_balance = game_rules.starting_balance.clone();
        let mut balances = HashMap::new();
        let mut player_map = HashMap::new();

        for player in players {
            balances.insert(player.token, starting_balance as i32);
            player_map.insert(player.token, player);
        }

        Bank {
            balances,
            players: player_map,
            game_assets,
            available_hotels: game_rules.hotel_count.clone(),
            available_houses: game_rules.house_count.clone(),
        }
    }

    pub fn get_metrics(&self) -> BankMetrics {
        // get the owner for each property
        // let property_owners = self.game_assets.cells.iter().map()

        // get all ownable cells
        let ownable_cells = self
            .game_assets
            .cells
            .iter()
            .map(|cell| cell.as_purchaseable())
            .filter(|optional_cell| optional_cell.is_some())
            .map(|purchasable| purchasable.unwrap());

        let upgradeable_cells = self
            .game_assets
            .cells
            .iter()
            .map(|cell| cell.as_upgradeable())
            .filter(|opt| opt.is_some())
            .map(|upgradeable| upgradeable.unwrap());

        // map each property to its owner
        let mut property_ownership: HashMap<i32, Option<PlayerToken>> = HashMap::new();
        let mut property_improvement_level: HashMap<i32, i32> = HashMap::new();

        for cell in ownable_cells {
            property_ownership.insert(cell.loc(), cell.owner());
        }

        for cell in upgradeable_cells {
            property_improvement_level.insert(cell.loc(), cell.get_improvement_tier());
        }

        BankMetrics {
            available_hotels: self.available_hotels,
            available_houses: self.available_houses,
            property_ownership,
            property_improvement_level,
        }
    }

    pub fn player_count(&self) -> usize {
        self.balances.len()
    }

    pub fn get_player_tokens(&self) -> Vec<PlayerToken> {
        self.balances.keys().cloned().collect()
    }

    /// Removes a player from the game by removing their balance from the bank
    /// and auctioning off their properties.
    pub fn remove_player(&mut self, player: PlayerToken) -> Result<(), GameError> {
        PlayerServices::remove_player(player, self)
    }

    pub fn get_player_balance(&self, player_token: PlayerToken) -> Result<i32, GameError> {
        PlayerServices::get_player_balance(player_token, self)
    }

    pub fn compute_player_net_worth(&self, player_token: PlayerToken) -> Result<i32, GameError> {
        PlayerServices::compute_player_net_worth(player_token, self)
    }

    pub fn assert_sufficient_player_funds(
        &self,
        player: &Player,
        amount: i32,
        reserve_percentage: Option<f32>,
    ) -> Result<(), GameError> {
        PlayerServices::assert_sufficient_funds(player, amount, reserve_percentage, self)
    }

    pub fn subtract_from_balance(
        &mut self,
        player: PlayerToken,
        amount: i32,
        reason: &str,
    ) -> Result<(), GameError> {
        PlayerServices::subtract_from_balance(player, amount, reason, self)
    }

    pub fn add_to_balance(&mut self, player: PlayerToken, amount: i32) -> Result<(), GameError> {
        PlayerServices::add_to_balance(player, amount, self)
    }

    pub fn player_to_player_transfer(
        &mut self,
        from: PlayerToken,
        to: PlayerToken,
        amount: i32,
    ) -> Result<(), GameError> {
        PlayerServices::player_to_player_transfer(from, to, amount, self)
    }

    pub fn purchase_property(
        &mut self,
        player: &Player,
        location: i32,
        bid: Option<i32>,
    ) -> Result<(), GameError> {
        PropertyServices::purchase_property(player, location, bid, self)
    }

    // pub fn lookup_property_cost(&self, property: i32) -> Result<i32, GameError> {
    //     PropertyServices::lookup_property_cost(property, self)
    // }

    pub fn mortgage_property(
        &mut self,
        player: PlayerToken,
        property: i32,
    ) -> Result<i32, GameError> {
        PropertyServices::mortgage_property(player, property, self)
    }

    pub fn unmortgage_property(
        &mut self,
        player: PlayerToken,
        property: i32,
    ) -> Result<(), GameError> {
        PropertyServices::unmortgage_property(player, property, self)
    }

    /// Returns a purchasable property back to the game, following a player's bankruptcy.
    /// It does this by setting the owner to None, and putting the property up for auction,
    /// per game rules.
    pub fn return_property(&mut self, property_location: i32) -> Result<(), GameError> {
        PropertyServices::return_property(property_location, self)
    }

    /// Given a color group for an improvable cell, this function
    /// improves each property in the group by 1 level.
    pub fn improve_property_group(
        &mut self,
        player: PlayerToken,
        color_group: &str,
    ) -> Result<(), GameError> {
        PropertyServices::improve_property_group(player, color_group, self)
    }

    /// Improves a single improvable property by one level.
    /// Increments the property's level by 1, then subtracts funds from the owner's account.
    /// Caution: Will attempt to raise player funds if insufficient cash is available, however, it is
    /// intended that player balance checking happens prior.
    // fn improve_property(&mut self, player: PlayerToken, property: i32) -> Result<(), GameError> {
    //     PropertyServices::improve_property(player, property, self)
    // }

    fn unimprove_property_group(
        &mut self,
        player: PlayerToken,
        color_group: &str,
    ) -> Result<i32, GameError> {
        PropertyServices::unimprove_property_group(player, color_group, self)
    }

    pub fn compute_current_rent(
        &self,
        location: i32,
        dice_roll: &DiceRoll,
    ) -> Result<i32, GameError> {
        PropertyServices::compute_current_rent(location, dice_roll, self)
    }

    pub fn auction_property(&mut self, location: i32) -> Result<(), GameError> {
        let game_cell = self
            .game_assets
            .cells
            .get_cell_at(location)
            .ok_or(GameError::CellNotFound(location));
        let property_cell = game_cell
            .unwrap()
            .as_purchaseable()
            .ok_or(GameError::OwnershipRequired);
        let property = property_cell.unwrap();
        let property_name = property.name();

        let mut bidders: Vec<Player> = self.players.values().cloned().collect();

        info!("Auctioning property {:?}", property_name);

        let initial_bid = property.mortgage_value();
        let mut highest_bidder: Option<Player> = None;
        let mut highest_bid: i32 = initial_bid;

        // handle the case of a single bidder
        if bidders.len() == 1 {
            let bidder = &bidders[0];
            // if the player can afford the property, purchase it
            let can_afford = PlayerServices::get_player_balance(bidder.token, self)? >= initial_bid;
            if can_afford {
                info!(
                    "Player {:?} bids ${:?} for {:?}",
                    bidder.token, initial_bid, property_name
                );
                highest_bidder = Some(bidder.clone());
                highest_bid = initial_bid;
            } else {
                info!(
                    "Player {:?} cannot afford to bid ${:?} for {:?}",
                    bidder.token, initial_bid, property_name
                );
            }
            bidders.clear();
        }

        // loop through the bidders until only one remains
        // a players bidding value is the highest bid + 10% of the perceived value
        // to a maximum of the perceived value.
        // if the player cannot afford the current bid, they are removed from the auction
        // the auction ends when there is only one player left.
        while bidders.len() > 1 {
            let mut next_bidders = Vec::new();
            for bidder in &bidders {
                // let bidder_accuracy = self.player_profiles[bidder].evaluation_accuracy;
                // let bidder_accuracy = self.players.get(bidder).expect("expected player").evaluation_accuracy;
                // let reserve_percentage = self.players.get(bidder).expect("expected player").cash_reserve_percentage;

                let reserve_percentage = bidder.cash_reserve_percentage;
                let bidder_accuracy = bidder.evaluation_accuracy;

                let max_offer =
                    Advisor::estimate_property_worth(location, bidder, bidder_accuracy, self)?;

                let bid_value: i32 = (highest_bid as f32 + 0.1 * max_offer as f32) as i32;

                info!(
                    "Player {:?} bids ${:?} for {:?} (max offer: ${:?})",
                    bidder.token, bid_value, property_name, max_offer
                );

                // a player will always bid if they can afford it
                // this is to maximize property acquisition.
                match PlayerServices::assert_sufficient_funds(
                    bidder,
                    bid_value,
                    Some(reserve_percentage),
                    self,
                ) {
                    Ok(_) => {
                        info!(
                            "Player {:?} bids ${:?} for {:?}",
                            bidder.token, bid_value, property_name
                        );
                        highest_bid = bid_value;
                        next_bidders.push(bidder.clone());
                    }
                    Err(_) => {
                        info!(
                            "Player {:?} declines to bid for {:?}",
                            bidder.token, property_name
                        );
                    }
                }
            }

            if next_bidders.len() == 1 {
                highest_bidder = Some(next_bidders[0].clone());
                break;
            }

            bidders = next_bidders;
        }

        if let Some(highest_bidder) = highest_bidder {
            return self.purchase_property(&highest_bidder, location, Some(highest_bid));
        } else {
            info!("No bids for {:?}", property_name);
            Ok(())
        }
    }
}
