use std::ops::Add;

use tracing::info;

use crate::{
    game_assets::{
        cell::{CellType, GameCell, Purchaseable},
        PlayerToken,
    },
    game_error::{GameError, GameErrorKind},
    player::Player,
};

use super::Bank;

macro_rules! player_transfer {
    ($from: expr, $to: expr, $amount: expr, $bank: expr) => {
        // throw an error if the amount is negative
        if $amount < 0 {
            return Err(GameError::NegativeTransferAmount($amount));
        }
        let from_balance = $bank
            .balances
            .get_mut(&$from)
            .ok_or(GameError::PlayerNotFound($from))?;

        *from_balance -= $amount;

        let to_balance = $bank
            .balances
            .get_mut(&$to)
            .ok_or(GameError::PlayerNotFound($to))?;
        *to_balance += $amount;

        info!(
            "Player {:?} transfers {:?} to player {:?}",
            $from, $amount, $to
        );
    };
}

pub struct PlayerServices {}

impl PlayerServices {
    pub fn remove_player(player: PlayerToken, bank: &mut Bank) -> Result<(), GameError> {
        if bank.balances.remove(&player).is_none() {
            return Err(GameError::PlayerNotFound(player));
        }

        // return all the player's assets
        let player_asset_locations: Vec<i32> = PlayerServices::get_player_assets(player, bank)
            .iter()
            .map(|asset| asset.loc())
            .collect();
        for asset_loc in player_asset_locations {
            bank.return_property(asset_loc)?;
        }

        // remove the player from the balances hashmap
        bank.balances.remove(&player);

        Ok(())
    }

    pub fn compute_player_net_worth(
        player_token: PlayerToken,
        bank: &Bank,
    ) -> Result<i32, GameError> {
        // get the players owned properties
        let player_owned_properties = bank.game_assets.cells.get_cells_by_owner(&player_token);

        let mut total_property_value = 0;
        for property in player_owned_properties {
            match property.cell_type() {
                CellType::Property => {
                    let property = property.as_upgradeable().unwrap();
                    let improvement_cost = property.total_improvement_cost() / 2;
                    // the property value is negative if the property is mortgaged
                    total_property_value += property.mortgage_value() + improvement_cost;

                    if property.get_mortgage() {
                        total_property_value -= property.mortgage_value();
                    }
                }
                CellType::Railroad | CellType::Utility => {
                    total_property_value += property.mortgage_value();

                    if property.as_purchaseable().unwrap().get_mortgage() {
                        total_property_value -= property.mortgage_value();
                    }
                }
                _ => panic!("Invalid cell type"),
            }
        }

        // add the player's cash balance to the total property value
        Ok(PlayerServices::get_player_balance(player_token, bank)?.add(total_property_value))
    }

    pub fn player_to_player_transfer(
        from: PlayerToken,
        to: PlayerToken,
        transfer_amount: i32,
        bank: &mut Bank,
    ) -> Result<(), GameError> {
        if transfer_amount < 0 {
            return Err(GameError::NegativeTransferAmount(transfer_amount));
        }

        let source_balance = PlayerServices::get_player_balance(from, bank)?;
        let shortfall = match transfer_amount > source_balance {
            true => transfer_amount - source_balance,
            false => 0,
        };

        if shortfall > 0 {
            // player pays what they can
            player_transfer!(from.clone(), to.clone(), source_balance, bank);
            // player then raises funds to pay the shortfall
            match PlayerServices::raise_funds(
                from,
                shortfall,
                &format!("To pay player {:?}", to),
                bank,
            ) {
                Err(e) => {
                    if e.kind() == GameErrorKind::InsufficientFunds {
                        // player has failed to raise funds to pay the shortfall
                        // the player should transfer what they did raise to the receiving player
                        // and then declare bankruptcy.
                        let updated_balance = PlayerServices::get_player_balance(from, bank)?;
                        let final_shortfall = transfer_amount - updated_balance;
                        player_transfer!(from.clone(), to.clone(), updated_balance, bank);
                        return Err(GameError::InsufficientFunds(from, final_shortfall));
                    } else {
                        // an unexpected error occurred
                        return Err(e);
                    }
                }
                Ok(_) => {
                    // player was able to raise funds to pay the shortfall
                    // pay the shortfall to the receiving player
                    player_transfer!(from.clone(), to.clone(), shortfall, bank);
                }
            }
        } else {
            player_transfer!(from.clone(), to.clone(), transfer_amount, bank);
        }

        Ok(())
    }

    pub fn add_to_balance(
        player: PlayerToken,
        amount: i32,
        bank: &mut Bank,
    ) -> Result<(), GameError> {
        let balance = bank
            .balances
            .get_mut(&player)
            .ok_or(GameError::PlayerNotFound(player))?;
        *balance += amount;

        info!(
            "Player {:?} receives {:?} from the bank.  Balance: {}",
            player, amount, balance
        );

        Ok(())
    }

    /// Subtracts the amount from the player's balance.
    /// If the player's balance is insufficient, the player must raise funds.
    /// If the player cannot raise funds, an error is returned.
    pub fn subtract_from_balance(
        player: PlayerToken,
        amount: i32,
        reason: &str,
        bank: &mut Bank,
    ) -> Result<(), GameError> {
        let player_balance = PlayerServices::get_player_balance(player, bank)?;
        let shortfall = if player_balance < amount {
            Some(amount - player_balance)
        } else {
            None
        };

        if let Some(shortfall) = shortfall {
            PlayerServices::raise_funds(player, shortfall, reason, bank)?;
        }

        // if the player cannot raise enough funds, return an error
        if PlayerServices::get_player_balance(player, bank)? < amount {
            return Err(GameError::InsufficientFunds(player, amount));
        }

        let balance = bank
            .balances
            .get_mut(&player)
            .ok_or(GameError::PlayerNotFound(player.clone()))?;
        *balance -= amount;

        info!(
            "Player {:?} pays {:?} to the bank.  New balance: {}. Reason: {}",
            player, amount, balance, reason
        );
        Ok(())
    }

    pub fn get_player_balance(player: PlayerToken, bank: &Bank) -> Result<i32, GameError> {
        let result = *bank
            .balances
            .get(&player)
            .ok_or(GameError::PlayerNotFound(player))?;
        Ok(result)
    }

    pub fn get_player_assets<'a>(player: PlayerToken, bank: &'a Bank) -> Vec<&'a dyn Purchaseable> {
        bank.game_assets.cells.get_cells_by_owner(&player)
    }

    pub fn get_player_assets_mut<'a>(
        player: PlayerToken,
        bank: &'a mut Bank,
    ) -> Vec<&'a mut dyn Purchaseable> {
        bank.game_assets.cells.get_cells_by_owner_mut(&player)
    }

    pub fn assert_sufficient_funds(
        player: &Player,
        amount: i32,
        reserve_percentage: Option<f32>,
        bank: &Bank,
    ) -> Result<(), GameError> {
        let reserve_amount = match reserve_percentage {
            Some(p) => {
                let balance = PlayerServices::get_player_balance(player.token, bank)? as f32;
                balance * p / 100.0
            }
            None => 0.0,
        }
        .floor() as i32;

        let player_balance =
            PlayerServices::get_player_balance(player.token, bank)? - reserve_amount;

        if player_balance < amount {
            return Err(GameError::InsufficientFunds(player.token, amount));
        } else {
            return Ok(());
        }
    }

    /// Raises funds for the player by mortgaging properties and selling improvements.
    /// If the player cannot raise enough funds, the function will recurse until the player
    /// has raised enough funds.
    /// If the player cannot raise enough funds, an error is returned.
    /// This will be handled by the player declaring bankruptcy.
    pub fn raise_funds(
        player: PlayerToken,
        shortfall: i32,
        reason: &str,
        bank: &mut Bank,
    ) -> Result<(), GameError> {
        if PlayerServices::get_player_balance(player, bank)? >= shortfall {
            return Ok(());
        }

        if shortfall < 0 {
            return Err(GameError::NegativeTransferAmount(shortfall));
        }

        info!(
            "Player {:?} needs to raise funds of {} (current balance: {}) Reason: {}",
            player,
            shortfall,
            PlayerServices::get_player_balance(player, bank)?,
            reason
        );

        // get all the player's assets
        let mut asset_locs: Vec<_> = PlayerServices::get_player_assets_mut(player, bank)
            .iter()
            .map(|a| a.loc())
            .collect();

        // sort the asset locations by mortgage value
        asset_locs.sort_by_key(|&loc| {
            bank.game_assets
                .cells
                .get_cell_at(loc)
                .unwrap()
                .as_purchaseable()
                .unwrap()
                .mortgage_value()
        });

        // filter the assets to only include unmortgaged and unimproved properties, railroads, and utilities
        let mortgageable_assets: Vec<_> = asset_locs
            .iter()
            .filter(|&&loc| {
                let cell = bank.game_assets.cells.get_cell_at(loc).unwrap();
                let cell_type = cell.cell_type();
                match cell_type {
                    CellType::Property => {
                        let property = cell.as_upgradeable().unwrap();
                        property.get_improvement_tier() == 0 && property.get_mortgage() == false
                    }
                    CellType::Railroad | CellType::Utility => {
                        let purchaseable = cell.as_purchaseable().unwrap();
                        purchaseable.get_mortgage() == false
                    }
                    _ => false,
                }
            })
            .collect();

        // raise funds by first mortgaging unimproved properties one at a time
        // and check if the player has raised enough funds
        for loc in mortgageable_assets {
            bank.mortgage_property(player, *loc)
                .expect("Failed to mortgage property");
            if PlayerServices::get_player_balance(player, bank)? >= shortfall {
                return Ok(());
            }
        }

        // check if the player has raised enough funds
        if PlayerServices::get_player_balance(player, bank)? >= shortfall {
            return Ok(());
        }

        // if the player has not raised enough funds, sell improvements
        let improved_assets = asset_locs
            .iter()
            .filter(|&&loc| {
                let cell = bank.game_assets.cells.get_cell_at(loc).unwrap();
                let cell_type = cell.cell_type();
                match cell_type {
                    CellType::Property => {
                        let property = cell.as_upgradeable().unwrap();
                        property.get_improvement_tier() > 0
                    }
                    _ => false,
                }
            })
            .copied()
            .collect::<Vec<i32>>();

        // err if the player has no assets
        if improved_assets.is_empty() {
            return Err(GameError::InsufficientFunds(player, shortfall));
        }

        for loc in improved_assets {
            let cell = bank.game_assets.cells.get_cell_at(loc).unwrap();

            let cell_type = cell.cell_type();

            let color_group = cell
                .as_upgradeable()
                .ok_or(GameError::UpgradeableRequired(cell_type))?
                .get_color_group();

            bank.unimprove_property_group(player, &color_group)
                .expect("Failed to unimprove property group");

            if PlayerServices::get_player_balance(player, bank)? >= shortfall {
                return Ok(());
            }
        }

        // if the player has not raised enough funds, recurse with the updated shortfall
        // let new_shortfall = shortfall - self.get_player_balance(player);

        if PlayerServices::get_player_balance(player, bank)? < shortfall {
            return PlayerServices::raise_funds(player, shortfall, reason, bank);
        } else {
            return Ok(());
        }

        // return self.raise_funds(player, new_shortfall, reason);
    }
}
