use tracing::info;

use crate::{
    game_assets::{
        cell::{CellType, GameCell},
        dice::DiceRoll,
        PlayerToken,
    },
    game_error::GameError, player::Player,
};

use super::Bank;

pub struct PropertyServices {}

impl PropertyServices {
    pub fn lookup_property_cost(property: i32, bank: &Bank) -> Result<i32, GameError> {
        let property_cell = bank
            .game_assets
            .cells
            .get_cell_at(property)
            .ok_or(GameError::CellNotFound(property))?;

        let cell_type = property_cell.cell_type();
        let purchaseable = property_cell
            .as_purchaseable()
            .ok_or(GameError::PurchaseableRequired(cell_type))?;

        Ok(purchaseable.cost())
    }

    pub fn purchase_property(
        player: &Player,
        property: i32,
        bid: Option<i32>,
        bank: &mut Bank,
    ) -> Result<(), GameError> {
        let cost = bid.unwrap_or_else(|| {
            PropertyServices::lookup_property_cost(property, bank)
                .expect("Failed to lookup property cost")
        });

        bank.subtract_from_balance(player.token, cost, &format!("To purchase property {}", property))?;

        let property_cell = bank
            .game_assets
            .cells
            .get_cell_at_mut(property)
            .ok_or(GameError::CellNotFound(property))?;

        let cell_type = property_cell.cell_type();

        let purchaseable_property = property_cell
            .as_purchaseable_mut()
            .ok_or(GameError::PurchaseableRequired(cell_type))?;

        purchaseable_property.set_owner(Some(player.token));

        info!(
            "Player {:?} purchases property {:?} for {:?}",
            player.token, property, cost
        );

        Ok(())
    }

    pub fn mortgage_property(
        player: PlayerToken,
        property: i32,
        bank: &mut Bank,
    ) -> Result<i32, GameError> {
        let property_cell = bank
            .game_assets
            .cells
            .get_cell_at_mut(property)
            .ok_or(GameError::CellNotFound(property))?;

        let cell_type = property_cell.cell_type();
        let purchaseable_property = property_cell
            .as_purchaseable_mut()
            .ok_or(GameError::InvalidCellType(cell_type))?;

        // ensure the property is owned by the player
        if let Some(owner) = purchaseable_property.owner() {
            if owner != player {
                return Err(GameError::OwnershipRequired);
            }
        } else {
            // if the property is not owned, return an error
            return Err(GameError::OwnershipRequired);
        }

        // if the property is upgradeable and has improvements, return an error
        if let Some(upgradeable) = purchaseable_property.as_upgradeable() {
            if upgradeable.get_improvement_tier() > 0 {
                return Err(GameError::MinImprovementsRequired);
            }
        }

        // ensure the property is not already mortgaged
        if purchaseable_property.get_mortgage() {
            return Err(GameError::UnMortgageRequired);
        }

        let mortgage_value = purchaseable_property.mortgage_value();
        info!(
            "Player {:?} mortgages property {:?} for {}",
            player, property, mortgage_value
        );
        purchaseable_property.mortgage();
        bank.add_to_balance(player, mortgage_value)?;

        Ok(mortgage_value)
    }

    pub fn unmortgage_property(
        player: PlayerToken,
        property: i32,
        bank: &mut Bank,
    ) -> Result<(), GameError> {
        let un_mortgage_value;
        {
            let property_cell = bank
                .game_assets
                .cells
                .get_cell_at_mut(property)
                .ok_or(GameError::CellNotFound(property))?;

            let cell_type = property_cell.cell_type();
            let purchaseable = property_cell
                .as_purchaseable_mut()
                .ok_or(GameError::PurchaseableRequired(cell_type))?;

            // mortgage value + 10%
            let mortgage_value = purchaseable.mortgage_value();
            un_mortgage_value = mortgage_value + (mortgage_value / 10);
        }

        bank.subtract_from_balance(
            player,
            un_mortgage_value,
            &format!("To un-mortgage property {}", property),
        )?;

        let property_cell = bank
            .game_assets
            .cells
            .get_cell_at_mut(property)
            .ok_or(GameError::CellNotFound(property))?;

        let cell_type = property_cell.cell_type();
        let purchaseable = property_cell
            .as_purchaseable_mut()
            .ok_or(GameError::PurchaseableRequired(cell_type))?;

        purchaseable.unmortgage();
        Ok(())
    }

    /// Returns a purchasable property back to the game, following a player's bankruptcy.
    /// It does this by setting the owner to None, and putting the property up for auction,
    /// per game rules.
    pub fn return_property(property_location: i32, bank: &mut Bank) -> Result<(), GameError> {
        let property_cell = bank
            .game_assets
            .cells
            .get_cell_at_mut(property_location)
            .ok_or(GameError::CellNotFound(property_location))?;

        let cell_type = property_cell.cell_type();
        let purchaseable = property_cell
            .as_purchaseable_mut()
            .ok_or(GameError::PurchaseableRequired(cell_type))?;

        purchaseable.set_owner(None);

        // if there is more than one player, auction the property
        if bank.player_count() > 1 {
            bank.auction_property(property_location)?;
        }
        Ok(())
    }

    /// Given a color group for an improvable cell, this function
    /// improves each property in the group by 1 level.
    pub fn improve_property_group(
        player: PlayerToken,
        color_group: &str,
        bank: &mut Bank,
    ) -> Result<(), GameError> {
        let color_group_locations = bank
            .game_assets
            .cells
            .get_cells_by_color_group(color_group)
            .iter()
            .map(|cell| cell.loc())
            .collect::<Vec<i32>>();

        // does the upgrade require houses or hotels?
        let current_improvement_level = bank
            .game_assets
            .cells
            .get_cell_at(color_group_locations[0])
            .unwrap()
            .as_upgradeable()
            .expect("Expected upgradeable property")
            .get_improvement_tier();
        let required_upgrades = color_group_locations.len() as i32;

        let has_sufficient_resources = match current_improvement_level {
            0..=4 => {
                // does the bank have enough houses?
                Ok(bank.available_houses >= required_upgrades)
            }
            // if the current lvl is 5, then the player has 4 houses
            // on each and needs a hotel.
            5 => Ok(bank.available_hotels >= required_upgrades),
            invalid => {
                // invalid improvement level
                Err(GameError::InvalidImprovementTier(invalid))
            }
        };

        if has_sufficient_resources? {
            for property in color_group_locations {
                PropertyServices::improve_property(player, property, bank)?;
            }
            Ok(())
        } else {
            Err(GameError::InsufficientResources)
        }
    }

    /// Improves a single improvable property by one level.
    /// Increments the property's level by 1, then subtracts funds from the owner's account.
    /// Caution: Will attempt to raise player funds if insufficient cash is available, however, it is
    /// intended that player balance checking happens prior.
    pub fn improve_property(
        player: PlayerToken,
        property: i32,
        bank: &mut Bank,
    ) -> Result<(), GameError> {
        let improvement_cost;
        {
            let property_cell = bank
                .game_assets
                .cells
                .get_cell_at_mut(property)
                .ok_or(GameError::CellNotFound(property))?;
            let cell_type = property_cell.cell_type();
            let property = property_cell
                .as_upgradeable_mut()
                .ok_or(GameError::UpgradeableRequired(cell_type))?;

            let current_improvement_tier = property.get_improvement_tier();

            if current_improvement_tier == 6 {
                return Err(GameError::MaxImprovementsReached);
            }

            // houses or hotels?
            if current_improvement_tier == 5 {
                // hotels
                bank.available_hotels -= 1;
            } else {
                bank.available_houses -= 1;
            }

            improvement_cost = property.next_improvement_cost();
            property.set_improvement_tier(current_improvement_tier + 1);
        }

        bank.subtract_from_balance(
            player,
            improvement_cost,
            &format!("Purchase improvement for property {}", property),
        )
    }

    pub fn unimprove_property_group(
        player_token: PlayerToken,
        color_group: &str,
        bank: &mut Bank,
    ) -> Result<i32, GameError> {
        let color_group_locations = bank
            .game_assets
            .cells
            .get_cells_by_color_group(color_group)
            .iter()
            .map(|cell| cell.loc())
            .collect::<Vec<i32>>();

        let mut total_received = 0;

        for property in color_group_locations {
            match PropertyServices::unimprove_property(player_token, property, bank) {
                Ok(amount_received) => total_received += amount_received,
                Err(GameError::MinImprovementsReached) => (),
                Err(e) => panic!("Unexpected error: {:?}", e),
            }
        }

        Ok(total_received)
    }

    pub fn unimprove_property(
        player_token: PlayerToken,
        property: i32,
        bank: &mut Bank,
    ) -> Result<i32, GameError> {
        let improvement_cost;
        {
            let property_cell = bank
                .game_assets
                .cells
                .get_cell_at_mut(property)
                .ok_or(GameError::CellNotFound(property))?;
            let property = property_cell
                .as_upgradeable_mut()
                .ok_or(GameError::CellNotFound(property))?;

            improvement_cost = property.next_improvement_cost();

            let current_improvement_tier = property.get_improvement_tier();

            if property.get_improvement_tier() == 0 {
                return Err(GameError::MinImprovementsReached);
            }

            // are we returning houses or hotels back to the bank
            if current_improvement_tier == 6 {
                bank.available_hotels += 1;
            } else {
                bank.available_houses += 1;
            }

            property.set_improvement_tier(current_improvement_tier - 1);
        }

        // the amount received is half the improvement cost
        let received = improvement_cost / 2;

        info!(
            "Player {:?} un-improves property {} for {}",
            player_token, property, received
        );
        bank.add_to_balance(player_token, received)?;
        Ok(received)
    }

    pub fn compute_current_rent(
        location: i32,
        dice_roll: &DiceRoll,
        bank: &Bank,
    ) -> Result<i32, GameError> {
        let property_cell = bank
            .game_assets
            .cells
            .get_cell_at(location)
            .ok_or(GameError::CellNotFound(location))?;

        let cell_type = property_cell.cell_type().clone();
        let purchaseable_cell = property_cell
            .as_purchaseable()
            .ok_or(GameError::PurchaseableRequired(cell_type))?;

        // different cell types have different rent calculation methods
        match purchaseable_cell.cell_type() {
            CellType::Property => {
                // what is the current improvement level?
                let property_cell = purchaseable_cell.as_upgradeable().unwrap();
                let rent_values = property_cell.get_rent();
                let improvement_tier = property_cell.get_improvement_tier();
                // determine rent based on improvement tier
                match improvement_tier {
                    0 => {
                        // does the player have a monopoly on the color group?
                        let color_group = property_cell.get_color_group();
                        // get all properties in the color group
                        let color_group_properties: Vec<_> = bank
                            .game_assets
                            .cells
                            .get_cells_by_color_group(&color_group);

                        // check if the player owns each property in the color group
                        let mut has_monopoly = true;
                        for property in color_group_properties {
                            if property.owner().as_ref()
                                != Some(&purchaseable_cell.owner().unwrap())
                            {
                                has_monopoly = false;
                                break;
                            }
                        }
                        // if the player has a monopoly, double the rent at tier 0
                        if has_monopoly {
                            return Ok(rent_values[0] * 2);
                        } else {
                            return Ok(rent_values[0]);
                        }
                    }
                    1..=5 => return Ok(rent_values[improvement_tier as usize]),

                    _ => return Err(GameError::InvalidImprovementTier(improvement_tier)),
                }
            }
            CellType::Railroad => {
                // get the owner of the property
                let owner = purchaseable_cell.owner().expect("Railroad has no owner");
                // the rent is calculated based on the number of railroads owned by the same player
                let owned_railroads_count = bank
                    .game_assets
                    .cells
                    .get_cells_by_owner(&owner)
                    .iter()
                    .filter(|cell| cell.cell_type() == CellType::Railroad)
                    .count();

                // if owned railroad count is 0, then something is wrong
                return Ok(purchaseable_cell.get_rent()[owned_railroads_count - 1]);
            }
            CellType::Utility => {
                // get the owner of the property
                let owner = purchaseable_cell.owner().unwrap();
                // the rent is calculated based on the number of other utilities owned by the same player
                let cells_by_owner = bank.game_assets.cells.get_cells_by_owner(&owner);

                let owned_utilities: Vec<_> = cells_by_owner
                    .iter()
                    .filter(|cell| cell.cell_type() == CellType::Utility)
                    .collect();

                // the rent tier is a dice-roll multiplier
                let rent_tier = purchaseable_cell.get_rent()[owned_utilities.len() - 1];

                return Ok(rent_tier * dice_roll.total);
            }
            _ => return Err(GameError::InvalidCellType(cell_type.clone())),
        }
    }
}
